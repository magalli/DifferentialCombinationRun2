# Run 2 Differential Combination Inputs

Input datacards and ROOT files for the full Run 2 differential combination:

- $H$ -> $\gamma \gamma$: https://gitlab.cern.ch/cms-hcg/cadi/hig-19-016.git
- $H$ -> $ZZ$: https://gitlab.cern.ch/cms-hcg/cadi/hig-19-001/-/tree/LegacyCards/differentialCards; updated to https://gitlab.cern.ch/cms-hcg/cadi/hig-21-009/-/tree/master/forCombination
- $H$ -> $\tau\tau$: https://gitlab.cern.ch/cms-hcg/cadi/hig-20-015
- $H$ -> $bb$: https://gitlab.cern.ch/cms-hcg/cadi/hig-19-003/
- $H$ -> $WW$: https://gitlab.cern.ch/cms-hcg/cadi/hig-19-002/-/tree/master/

All these gitlab repos are submodules of this module. 

To update the repos to the latest releases, follow these commands (e.g. for HWW):
- ```cd Analyses/hig-19-002```
- ```git pull```
- ```cd ..```
- (no need to ```git add``` because git already keeps tracks of the updated submodules in ```.gitmodules```)
- ```git commit -m "meaningful message where the tag of the updated submodule is specified"```

The naming conventions are described [here](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsWG/HiggsDifferentialConventions).

## Where are the datacards located?
From now on, for the sake of not spending too much time in typing, $\gamma \gamma$ will be gg and $\tau \tau$ will be tt.

Every repo has a different organization. The combination datacards needed are located respectively:

### gg




### ZZ 

branch ```LegacyCards```, ./forCombination
```
.
|-- combined2016.txt.cmb
|-- combined2017.txt.cmb
`-- combined2018.txt.cmb
```
### tt
 this is the structure
```
.
|-- HiggsPt
|-- LeadingJetPt
`-- NJets
``` 
inside each observable directory there is a file called ```FinalCard_200721_*_.txt```

### bb 
```HJMINLO/cms_datacard_hig-19-003``` at the top level should be the only one needed.

### WW
this is the structure:
```
.
|-- njet
`-- ptH
```
inside each of them:
```
$ ls ptH/
fullmodel.txt  fullmodel_2016.txt  fullmodel_2017.txt  fullmodel_2018.txt  templates_2016.root  templates_2017.root  templates_2018.root
```

## Changes made to the analyses

### gg

### ZZ

### tt

### bb

### WW

A random '_hww' was added to the process. Operations performed:

- remove constraints b1429dfaa2e43075d5c504117ede88c787f52843
- replace "smH_hww" with "smH" in fullmodel.txt 19b76e7ad92cbd053d11598f13a59a7e861623d6
- perform the same operation on the ROOT files containing the template histograms 0b1ea358d7163a120c4b7ee8a365082cdecd7523 (for this, ```specific_scripts/rename_HWW.py``` has to be used)


## WIP Instructions and Stuff (since apparently having 10 thousand km long commands is the only way to go with Combine)

First of all, let's introduce some conventions that can be used in the code:

- decay channels will be: ```Hgg```, ```Hbb```, ```Hgg```, ```HWW```, ```HZZ```
- for now, total and partial combinations will have the full names of the decay channels (e.g. ```Hgg_Hbb```, ```HWW_Htt```, etc.); if it will be not practical, we will define something like (```"comb1": "Hgg_Hbb", "comb2": "HWW_Htt"```)
- even if for now we are mostly working with ```ptH```, we should prepend always the observable in order to not fuck up later
- whenever some datacard/workspace refers to **full Run2**, we don't prepend/append anything in the category name; if, on the other hand, it refers to a specific year we prepend the year itself to the category name
- open question: should we take into consideration the generation process (smH, ggH, xH) in the category naming convention? - since it's only for ptH, a simple ggH_ptH/smH_ptH is enough

- PhysicsModels in Combine: here is the proper syntax to pass an expression from the command line to ```text2workspace.py```:
```
text2workspace.py DifferentialCombinationRun2/Analyses/hig-19-002/ptH/fullmodel.txt -o DifferentialCombinationRun2/CombinedWorkspaces/smH_PTH/HWW.root -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel --PO map='.*smH_PTH_0_20:r_smH_PTH_0_20[1.0, -1.0, 4.0]' --PO map='.*smH_PTH_20_45:function_of_pois=expr::function_of_pois("@0", r_smH_PTH_0_20[1.0, -1.0, 4.0])'
```
it is important to notice that, on the other hand, something like this
```
text2workspace.py DifferentialCombinationRun2/Analyses/hig-19-002/ptH/fullmodel.txt -o DifferentialCombinationRun2/CombinedWorkspaces/smH_PTH/HWW.root -P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel --PO map='.*smH_PTH_20_45:function_of_pois=expr::function_of_pois("@0", r_smH_PTH_0_20[1.0, -1.0, 4.0])'
```
won't work, since ```r_smH_PTH_0_20``` has to be created first

## Useful Tools

Inside ```tools``` there are scripts to explore datacards in an acceptable way. Documentation is inside





# Interpretation

## Setup Madgraph

- download from [here](https://launchpad.net/mg5amcnlo) (Python3 version)
- use with conda environment ```DiffCombPostProcess```
- apparently, it doesn't need to be installed and it have to be run from inside the folder
- there are, however, some plugins that have to be installed (see instructions [here](https://twiki.cern.ch/twiki/bin/view/CMSPublic/MadgraphTutorial))
- two parts tutorial given by Ken Mimasu: [1](https://indico.cern.ch/event/1030115/) and [2](https://indico.cern.ch/event/1043867/)
- if using SMEFTatNLO, depending where you're running, the module might have to be downloaded specifically and put inside the ```models``` directory of madgraph (see [instructions](http://feynrules.irmp.ucl.ac.be/wiki/SMEFTatNLO))
- another tool worth trying is SMEFTsim, see [here](https://indico.cern.ch/event/982554/sessions/385534/#20210217) with the [recording](https://cds.cern.ch/record/2752555)
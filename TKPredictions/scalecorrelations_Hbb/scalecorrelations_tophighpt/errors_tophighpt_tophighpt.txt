# Generated on 22-09-21 11:09:03 by differentials/theory/scalecorrelation.py; current git commit: 9470de8 Fix paths and change commands options for interpretation scans
# Uncertainties in pb (*not* pb/GeV)
# Up        Down
+3.75360607 +3.97583402
+0.09212329 +0.10699296
+0.01487145 +0.01791141
+0.00093025 +0.00195156
+0.00020682 +0.00035998

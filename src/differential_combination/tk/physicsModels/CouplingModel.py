from HiggsAnalysis.CombinedLimit.PhysicsModel import *

from array import array
from time import sleep
import yaml
from yaml import Loader
import json

from MethodHandler import MethodHandler
methodhandler = MethodHandler([
    'differential_combination.tk.physicsModels.CouplingModelMethods.YieldParameters',
    'differential_combination.tk.physicsModels.CouplingModelMethods.Parametrization',
    'differential_combination.tk.physicsModels.CouplingModelMethods.CouplingDependentBRs',
    'differential_combination.tk.physicsModels.CouplingModelMethods.TheoryUncertainties',
    'differential_combination.tk.physicsModels.CouplingModelMethods.getYieldScale',
    # 'physicsModels.CouplingModelMethods.OtherStudies',
    ])

class CouplingModel( PhysicsModel ):
    ''' Model used to unfold differential distributions '''

    class CouplingModelError(Exception):
        pass

    def __init__(self):
        PhysicsModel.__init__(self)
        self.mHRange=[]
        self.mass = 125.38
        self.verbose = 1

        self.Persistence = []
        self.theories = []
        self.SMbinBoundaries = {}
        self.SMXS = []

        self.isOnlyHgg = False
        self.isOnlyHZZ = False

        self.do_BR_uncertainties  = False # Meant for the non-scaling
        self.BRs_kappa_dependent  = False
        self.freely_floating_BRs  = False
        
        self.add_scaling_ttH      = False
        self.add_scaling_bbH      = False

        self.includeLinearTerms   = False
        self.splitggH             = False
        self.MakeLumiScalable     = False
        self.FitBR                = False
        self.ProfileTotalXS       = False
        self.FitOnlyNormalization = False
        self.scale_with_mu_totalXS = False
        # self.DoBRUncertainties    = False

        self.SMXS_of_input_ws     = {}

        self.inc_xs_uncertainty   = None

        self.theoryUncertaintiesPassed = False
        self.correlationMatrixPassed   = False
        self.covarianceMatrixPassed    = False
        self.skipOverflowBinTheoryUncertainty = False

        self.manualExpBinBoundaries = []
        self.skipBins = []
        self.skipOverflowBin = False

        self.channel = None
        self.model_type = None

        self.SM_HIGG_DECAYS = [ "hww", "hzz", "hgg", "htt", "hbb", 'hzg', 'hmm', 'hcc', 'hgluglu' ]
        self.SM_HIGG_DECAYS.append( 'hss' )


        self.boolean_options = []
        self.make_boolean_option('radial_coord_for_ctcg')
        self.make_boolean_option('constrain_ratio_bb_ZZ')


    def make_boolean_option(self, option_name, default=False):
        self.boolean_options.append(option_name)
        setattr(self, option_name, default)


    def setPhysicsOptions( self, physOptions ):
        self.chapter( 'Starting model.setPhysicsOptions()' )

        for physOption in physOptions:
            optionName, optionValue = physOption.split('=',1)

            # First check the default list
            option_is_boolean = False
            for boolean_option_name in self.boolean_options:
                if optionName == boolean_option_name:
                    boolean = eval(optionValue)
                    if not isinstance(boolean, bool):
                        raise TypeError('Variable %s should be True or False' % optionName)
                    setattr(self, boolean_option_name, boolean)
                    option_is_boolean = True
                    break
            if option_is_boolean: continue

            if False: pass
            elif optionName == 'splitggH':
                self.splitggH = eval(optionValue)
            elif optionName == 'isOnlyHZZ':
                self.isOnlyHZZ = eval(optionValue)
            elif optionName == 'isOnlyHgg':
                self.isOnlyHgg = eval(optionValue)
            elif optionName == 'binBoundaries':
                with open(optionValue) as f:
                    self.manualExpBinBoundaries = yaml.load(f, Loader=Loader)
            elif optionName == 'verbose':
                self.verbose = int(optionValue)
            elif optionName == 'linearTerms':
                self.includeLinearTerms = eval(optionValue)
            elif optionName == 'higgsMassRange':
                self.mHRange = [ float(v) for v in optionValue.split(',') ]

            elif optionName == 'theoryUncertainties':
                with open(optionValue) as f:
                    dct = yaml.load(f, Loader=Loader)
                self.theoryUncertainties = {}
                for k, v in dct.items():
                    self.theoryUncertainties[k] = self.readErrorFile(v)
                self.theoryUncertaintiesPassed = True
            elif optionName == 'correlationMatrix':
                with open(optionValue) as f:
                    dct = yaml.load(f, Loader=Loader)
                self.correlationMatrix = {}
                for k, v in dct.items():
                    self.correlationMatrix[k] = self.readCorrelationMatrixFile(v)
                self.correlationMatrixPassed = True
            elif optionName == 'covarianceMatrix':
                self.covarianceMatrix = self.readCorrelationMatrixFile( optionValue )
                self.covarianceMatrixPassed = True

            elif optionName == 'channel':
                self.channel = optionValue
                if self.channel == "hgghzzhwwhtt":
                    self.decayChannels = ["hgg", "hzz", "hww", "htt"]
                elif self.channel == "hgghzzhwwhtthbb":
                    self.decayChannels = ["hgg", "hzz", "hww", "htt", "hbb"]
                elif self.channel == "hgghzzhwwhtthttboost":
                    self.decayChannels = ["hgg", "hzz", "hww", "htt", "httboost"]
                else:
                    self.decayChannels = [self.channel]
           
            elif optionName == 'model_type':
                self.model_type = optionValue

            elif optionName == 'inc_xs_uncertainty':
                self.inc_xs_uncertainty = float(optionValue)

            elif optionName == 'lumiScale':
                self.MakeLumiScalable = eval(optionValue)
            elif optionName == 'SMXS_of_input_ws':
                with open(optionValue) as f:
                    self.SMXS_of_input_ws = yaml.load(f, Loader=Loader)
            elif optionName == 'FitBR':
                self.FitBR = eval(optionValue)
            elif optionName == 'ProfileTotalXS':
                self.ProfileTotalXS = eval(optionValue)
            elif optionName == 'FitOnlyTotalXS':
                self.FitOnlyNormalization = eval(optionValue)
            elif optionName == 'scale_with_mu_totalXS':
                self.scale_with_mu_totalXS = eval(optionValue)
            # elif optionName == 'DoBRUncertainties':
            #     self.DoBRUncertainties = eval(optionValue)

            elif optionName == 'BRs_kappa_dependent':
                self.BRs_kappa_dependent = eval(optionValue)
            elif optionName == 'do_BR_uncertainties':
                self.do_BR_uncertainties = eval(optionValue)
            elif optionName == 'freely_floating_BRs':
                self.freely_floating_BRs = eval(optionValue)
            
            elif optionName == 'add_scaling_ttH':
                self.add_scaling_ttH = eval(optionValue)
                with open("/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TKPredictions/ttH.json", "rb") as f:
                    self.smxs_ttH = json.load(f)
            elif optionName == 'add_scaling_bbH':
                self.add_scaling_bbH = eval(optionValue)

            elif optionName == 'theory':
                # Syntax: --PO theory=[ct=1,cg=1,file=some/path/.../] , brackets optional
                # Delete enclosing brackets if present
                if optionValue.startswith('['): optionValue = optionValue[1:]
                if optionValue.endswith(']'): optionValue = optionValue[:-1]
                theoryDict = {                
                    'couplings'      : {},
                    'binBoundaries'  : [],
                    'crosssection'   : [],
                    'ratios'         : [],
                    }
                for component in optionValue.split(','):
                    couplingName, couplingValue = component.split('=',1)
                    # Only exception is file, which is the file to read the shape from
                    if couplingName == 'file':
                        theoryFile = couplingValue
                        theoryDict['binBoundaries'], theoryDict['ratios'], theoryDict['crosssection'] = self.readTheoryFile(theoryFile)
                        continue
                    theoryDict['couplings'][couplingName] = float(couplingValue)
                self.theories.append( theoryDict )
            elif optionName == 'SM':
                # Delete enclosing brackets if present
                if optionValue.startswith('['): optionValue = optionValue[1:]
                if optionValue.endswith(']'): optionValue = optionValue[:-1]
                self.SMDict = {                
                    'couplings'      : {},
                    'binBoundaries'  : [],
                    'crosssection'   : [],
                    }
                for component in optionValue.split(','):
                    couplingName, couplingValue = component.split('=',1)
                    # Only exception is file, which is the file to read the shape from
                    if couplingName == 'file':
                        self.SMDict['binBoundaries'], dummy, self.SMDict['crosssection'] = self.readTheoryFile( couplingValue )
                        continue
                    self.SMDict['couplings'][couplingName] = float(couplingValue)
                # SM cross section extended with underflow and overflow bin
                # self.SMXS = [ self.SMDict['crosssection'][0] ] + self.SMDict['crosssection'] + [ self.SMDict['crosssection'][-1] ]
                self.SMXS = [ 0.0 ] + self.SMDict['crosssection']

            else:
                print 'Unknown option \'{0}\'; skipping'.format(optionName)
                continue

        # Minor checks before continuing
        if self.FitBR and self.FitRatioOfBRs:
            raise self.CouplingModelError( 'Options FitBR and FitRatioOfBRs are exclusive!' )
        if not(self.SMXS_of_input_ws is None) and not self.FitOnlyNormalization:
            for (k, v), (k2, v2) in zip(self.SMXS_of_input_ws.items(), self.manualExpBinBoundaries.items()):
                if len(v) != len(v2) - 1:
                    raise(self.CouplingModelError('Length of SMXS_of_input_ws and manualExpBinBoundaries do not match for key {}!'.format(k)))

        if self.BRs_kappa_dependent and not(self.do_BR_uncertainties):
            print 'WARNING: Using kappa-dependent BRs, but no uncertainty associated with the BRs!'


    def doParametersOfInterest(self):
        print(self.manualExpBinBoundaries)
        print(self.decayChannels)
        self.chapter('Starting model.doParametersOfInterest()')
        if len(self.decayChannels) == 1:
            to_pop = [x for x in ["hgg", "hzz", "hww", "htt", "hbb", "httboost", "hbbvbf"] if x not in self.decayChannels]
            for x in to_pop:
                self.manualExpBinBoundaries.pop(x)
                self.theoryUncertainties.pop(x)
                self.correlationMatrix.pop(x)
                self.SMXS_of_input_ws.pop(x)
        self.setMH()

        if self.FitOnlyNormalization:
            self.makeParametrizationsFromTheory()

            if self.BRs_kappa_dependent:
                # Uncertainties depend on do_BR_uncertainties flag!
                self.implement_scaling_BRs()

            self.MakeTotalXSExpressions()
            self.modelBuilder.out.defineSet( 'POI', ','.join(self.couplings) )


        else:
            self.figureOutBinning()
            self.makeParametrizationsFromTheory()

            if self.scale_with_mu_totalXS or self.add_scaling_bbH:
                self.MakeTotalXSExpressions()

            if self.freely_floating_BRs:
                self.implement_freely_floating_BRs()
            elif self.BRs_kappa_dependent:
                # Uncertainties here depend on do_BR_uncertainties flag!
                self.implement_scaling_BRs()
            elif self.do_BR_uncertainties:
                # If do_BR_uncertainties but not BRs_kappa_dependent, do the non-scaling
                # so the BR uncertainties at least are implemented
                self.implement_nonscaling_BR_uncertainties()

            # Needs more thinking
            # if self.ProfileTotalXS:
            #     self.MakeTotalXSExpressions()

            self.defineYieldParameters()
            self.addTheoryUncertaintyNuisances()

        self.chapter( 'Starting model.getYieldScale()' )

    def figureOutBinning( self ):
        bins    = self.DC.bins
        signals = self.DC.signals
        signals = [ s for s in signals if not 'OutsideAcceptance' in s ]
        productionModes = list(set([ s.split('_')[0] for s in signals ]))
        if self.splitggH: signals = [ s for s in signals if 'ggH' in s ]
        signals.sort( key = lambda n: float(
            n.split('_')[2].replace('p','.').replace('m','-').replace('GT','').replace('GE','').replace('LT','').replace('LE','') ) )

        if len(signals) == 0:
            raise RuntimeError(
                'len(signals) == 0. Signals in self.DC.signals: {0}'
                .format(self.DC.signals)
                )
        try:
            productionMode, observableName = signals[0].split('_')[:2]
        except IndexError:
            print 'ERROR determining production mode and observable name from signal {0}'.format(signals[0])
            raise

        if len(self.manualExpBinBoundaries) == 0:
            raise self.CouplingModelError(
                'Specify \'--PO binBoundaries=0,15,...\' on the command line.'
                'Automatic boundary determination no longer supported.'
                )
        expBinBoundaries = self.manualExpBinBoundaries
        print('ExpBinBoundaries: ', expBinBoundaries)
        nExpBins = {}
        for k, v in expBinBoundaries.items():
            nExpBins[k] = len(v) - 1
        if self.verbose: print 'Taking manually specified bin boundaries:', expBinBoundaries
        
        self.allProcessBinBoundaries = []
        for signal in signals:
            if 'OutsideAcceptance' in signal:
                continue
            for bound in signal.split('_')[2:]:
                bound = bound.replace('p','.').replace('m','-').replace('GT','').replace('GE','').replace('LT','').replace('LE','')
                self.allProcessBinBoundaries.append( float(bound) )
        self.allProcessBinBoundaries = list(set(self.allProcessBinBoundaries))
        self.allProcessBinBoundaries.sort()

        # Attach to class so they are accesible in other methods
        self.expBinBoundaries = expBinBoundaries
        self.nExpBins         = nExpBins
        self.signals          = signals
        self.productionMode   = productionMode
        self.observableName   = observableName
        print(self.expBinBoundaries)

        expBinBoundarySet = []
        for decay_channel, exp_bin_boundaries in self.expBinBoundaries.items():
            for i, exp_bin_boundary in enumerate(exp_bin_boundaries):
                #if self.skipOverflowBin and expBinBoundary == expBinBoundaries[-1]: continue
                self.modelBuilder.doVar('expBinBound_{0}_{1}[{2}]'.format(decay_channel, i, exp_bin_boundary))
                expBinBoundarySet.append('expBinBound_{0}_{1}'.format(decay_channel, i))
        print(expBinBoundarySet)
        self.modelBuilder.out.defineSet('expBinBoundaries', ','.join(expBinBoundarySet))

    def setMH( self ):
        if self.modelBuilder.out.var("MH"):
            if len(self.mHRange):
                print 'MH will be left floating within', self.mHRange[0], 'and', self.mHRange[1]
                self.modelBuilder.out.var("MH").setRange(float(self.mHRange[0]),float(self.mHRange[1]))
                self.modelBuilder.out.var("MH").setConstant(False)
            else:
                print 'MH will be assumed to be', self.mass
                self.modelBuilder.out.var("MH").removeRange()
                self.modelBuilder.out.var("MH").setVal(self.mass)
        else:
            if len(self.mHRange):
                print 'MH will be left floating within', self.mHRange[0], 'and', self.mHRange[1]
                self.modelBuilder.doVar("MH[%s,%s]" % (self.mHRange[1],self.mHRange[1]))
            else:
                print 'MH (not there before) will be assumed to be', self.mass
                self.modelBuilder.doVar("MH[%g]" % self.mass)

    def chapter( self, text, indent=0 ):
        if self.verbose:
            print '\n{tabs}{line}\n{tabs}{text}\n'.format(
                tabs = '    ' * indent,
                line = '-' * 70,
                text = text
                )

    def distinguish_between_decay_channels(self):
        if self.FitBR or self.do_BR_uncertainties or self.BRs_kappa_dependent or self.freely_floating_BRs:
            return True
        return False

    def get_decay_channels(self):
        if self.isOnlyHgg:
            return ['hgg']
        if self.isOnlyHZZ:
            return ['hzz']
        decayChannels = []
        for b in self.DC.bins:
            match = re.match( r'(h[a-zA-Z]+)_', b )
            if match:
                decayChannels.append(match.group(1))
        decayChannels = list(set(decayChannels))
        return decayChannels

    def BinIsHgg( self, bin ):
        # If the input is hardcoded to be only hzz or hgg:
        if self.isOnlyHgg:
            return True
        if self.isOnlyHZZ:
            return False
        
        # This is not very robust coding
        if bin.startswith('hgg_'):
            # if self.verbose: print '    Bin \'{0}\' is classified as a hgg bin!'.format( bin )
            return True
        elif bin.startswith('hzz_'):
            # if self.verbose: print '    Bin \'{0}\' is classified as a hzz bin!'.format( bin )
            return False
        else:
            raise self.CouplingModelError( 'Bin \'{0}\' can not be classified as either a hgg or hzz bin'.format(bin) )

#____________________________________________________________________
# Finalize
methodhandler.make_methods(CouplingModel)
couplingModel = CouplingModel()

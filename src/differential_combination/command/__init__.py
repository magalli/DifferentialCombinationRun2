from .SM import SM_commands
from .SMEFT import SMEFT_commands

commands = {"SM": SM_commands, "SMEFT": SMEFT_commands}


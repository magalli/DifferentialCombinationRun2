from .smH_PTH import Hgg as smH_PTH_Hgg
from .smH_PTH import Hgg_statonly as smH_PTH_Hgg_statonly
from .smH_PTH import Hgg_asimov as smH_PTH_Hgg_asimov
from .smH_PTH import HZZ as smH_PTH_HZZ
from .smH_PTH import HZZ_statonly as smH_PTH_HZZ_statonly
from .smH_PTH import HZZ_asimov as smH_PTH_HZZ_asimov
from .smH_PTH import HZZ_asimov_statonly as smH_PTH_HZZ_asimov_statonly
from .smH_PTH import HWW as smH_PTH_HWW
from .smH_PTH import HWW_statonly as smH_PTH_HWW_statonly
from .smH_PTH import HWW_asimov as smH_PTH_HWW_asimov
from .smH_PTH import Htt as smH_PTH_Htt
from .smH_PTH import Htt_statonly as smH_PTH_Htt_statonly
from .smH_PTH import Htt_asimov as smH_PTH_Htt_asimov
from .smH_PTH import Hbb as smH_PTH_Hbb
from .smH_PTH import Hbb_statonly as smH_PTH_Hbb_statonly
from .smH_PTH import Hbb_asimov as smH_PTH_Hbb_asimov
from .smH_PTH import Hbb_asimov_statonly as smH_PTH_Hbb_asimov_statonly
from .smH_PTH import HbbVBF as smH_PTH_HbbVBF
from .smH_PTH import HbbVBF_statonly as smH_PTH_HbbVBF_statonly
from .smH_PTH import HbbVBF_asimov as smH_PTH_HbbVBF_asimov
from .smH_PTH import HttBoost as smH_PTH_HttBoost
from .smH_PTH import HttBoost_statonly as smH_PTH_HttBoost_statonly
from .smH_PTH import HttBoost_asimov as smH_PTH_HttBoost_asimov
from .smH_PTH import HggHZZ as smH_PTH_HggHZZ
from .smH_PTH import HggHZZ_statonly as smH_PTH_HggHZZ_statonly
from .smH_PTH import HggHZZ_asimov as smH_PTH_HggHZZ_asimov
from .smH_PTH import HggHZZHWW as smH_PTH_HggHZZHWW
from .smH_PTH import HggHZZHWW_statonly as smH_PTH_HggHZZHWW_statonly
from .smH_PTH import HggHZZHWW_asimov as smH_PTH_HggHZZHWW_asimov
from .smH_PTH import HggHZZHWWHtt as smH_PTH_HggHZZHWWHtt
from .smH_PTH import HggHZZHWWHtt_statonly as smH_PTH_HggHZZHWWHtt_statonly
from .smH_PTH import HggHZZHWWHtt_asimov as smH_PTH_HggHZZHWWHtt_asimov
from .smH_PTH import HggHZZHWWHttHbb as smH_PTH_HggHZZHWWHttHbb
from .smH_PTH import HggHZZHWWHttHbb_statonly as smH_PTH_HggHZZHWWHttHbb_statonly
from .smH_PTH import HggHZZHWWHttHbb_asimov as smH_PTH_HggHZZHWWHttHbb_asimov
from .smH_PTH import (
    HggHZZHWWHttHbb_asimov_statonly as smH_PTH_HggHZZHWWHttHbb_asimov_statonly,
)
from .smH_PTH import HggHZZHWWHttHbbVBF as smH_PTH_HggHZZHWWHttHbbVBF
from .smH_PTH import HggHZZHWWHttHbbVBF_statonly as smH_PTH_HggHZZHWWHttHbbVBF_statonly
from .smH_PTH import HggHZZHWWHttHbbVBF_asimov as smH_PTH_HggHZZHWWHttHbbVBF_asimov
from .smH_PTH import HggHZZHWWHttHbbVBFHttBoost as smH_PTH_HggHZZHWWHttHbbVBFHttBoost
from .smH_PTH import (
    HggHZZHWWHttHbbVBFHttBoost_statonly as smH_PTH_HggHZZHWWHttHbbVBFHttBoost_statonly,
)
from .smH_PTH import (
    HggHZZHWWHttHbbVBFHttBoost_asimov as smH_PTH_HggHZZHWWHttHbbVBFHttBoost_asimov,
)
from .smH_PTH import HggHZZinclusive as smH_PTH_HggHZZinclusive

from .Njets import Hgg as Njets_Hgg
from .Njets import Hgg_statonly as Njets_Hgg_statonly
from .Njets import HWW as Njets_HWW
from .Njets import HWW_statonly as Njets_HWW_statonly
from .Njets import Htt as Njets_Htt
from .Njets import HggHWW as Njets_HggHWW
from .Njets import HggHWW_statonly as Njets_HggHWW_statonly
from .Njets import HggHWWHtt as Njets_HggHWWHtt
from .Njets import HggHWWHtt_statonly as Njets_HggHWWHtt_statonly

from .yH import Hgg as yH_Hgg
from .yH import HZZ as yH_HZZ
from .yH import HZZ_statonly as yH_HZZ_statonly
from .yH import HZZ_asimov as yH_HZZ_asimov
from .yH import HggHZZ as yH_HggHZZ
from .yH import HggHZZ_statonly as yH_HggHZZ_statonly
from .yH import Hgginclusive as yH_Hgginclusive
from .yH import HZZinclusive as yH_HZZinclusive
from .yH import HggHZZinclusive as yH_HggHZZinclusive
from .yH import HggHZZinclusive_statonly as yH_HggHZZinclusive_statonly
from .yH import HggHZZinclusive_asimov as yH_HggHZZinclusive_asimov

from .smH_PTJ0 import Hgg as smH_PTJ0_Hgg
from .smH_PTJ0 import Hgg_statonly as smH_PTJ0_Hgg_statonly
from .smH_PTJ0 import Hgg_asimov as smH_PTJ0_Hgg_asimov
from .smH_PTJ0 import Hgg_asimov_statonly as smH_PTJ0_Hgg_asimov_statonly

SM_commands = {
    "smH_PTH": {
        "Hgg": smH_PTH_Hgg,
        "Hgg_statonly": smH_PTH_Hgg_statonly,
        "Hgg_asimov": smH_PTH_Hgg_asimov,
        "HZZ": smH_PTH_HZZ,
        "HZZ_statonly": smH_PTH_HZZ_statonly,
        "HZZ_asimov": smH_PTH_HZZ_asimov,
        "HZZ_asimov_statonly": smH_PTH_HZZ_asimov_statonly,
        "HWW": smH_PTH_HWW,
        "HWW_statonly": smH_PTH_HWW_statonly,
        "HWW_asimov": smH_PTH_HWW_asimov,
        "Htt": smH_PTH_Htt,
        "Htt_statonly": smH_PTH_Htt_statonly,
        "Htt_asimov": smH_PTH_Htt_asimov,
        "Hbb": smH_PTH_Hbb,
        "Hbb_statonly": smH_PTH_Hbb_statonly,
        "Hbb_asimov": smH_PTH_Hbb_asimov,
        "Hbb_asimov_statonly": smH_PTH_Hbb_asimov_statonly,
        "HbbVBF": smH_PTH_HbbVBF,
        "HbbVBF_statonly": smH_PTH_HbbVBF_statonly,
        "HbbVBF_asimov": smH_PTH_HbbVBF_asimov,
        "HttBoost": smH_PTH_HttBoost,
        "HttBoost_statonly": smH_PTH_HttBoost_statonly,
        "HttBoost_asimov": smH_PTH_HttBoost_asimov,
        "HggHZZ": smH_PTH_HggHZZ,
        "HggHZZ_statonly": smH_PTH_HggHZZ_statonly,
        "HggHZZ_asimov": smH_PTH_HggHZZ_asimov,
        "HggHZZHWW": smH_PTH_HggHZZHWW,
        "HggHZZHWW_statonly": smH_PTH_HggHZZHWW_statonly,
        "HggHZZHWW_asimov": smH_PTH_HggHZZHWW_asimov,
        "HggHZZHWWHtt": smH_PTH_HggHZZHWWHtt,
        "HggHZZHWWHtt_statonly": smH_PTH_HggHZZHWWHtt_statonly,
        "HggHZZHWWHtt_asimov": smH_PTH_HggHZZHWWHtt_asimov,
        "HggHZZHWWHttHbb": smH_PTH_HggHZZHWWHttHbb,
        "HggHZZHWWHttHbb_statonly": smH_PTH_HggHZZHWWHttHbb_statonly,
        "HggHZZHWWHttHbb_asimov": smH_PTH_HggHZZHWWHttHbb_asimov,
        "HggHZZHWWHttHbb_asimov_statonly": smH_PTH_HggHZZHWWHttHbb_asimov_statonly,
        "HggHZZHWWHttHbbVBF": smH_PTH_HggHZZHWWHttHbbVBF,
        "HggHZZHWWHttHbbVBF_statonly": smH_PTH_HggHZZHWWHttHbbVBF_statonly,
        "HggHZZHWWHttHbbVBF_asimov": smH_PTH_HggHZZHWWHttHbbVBF_asimov,
        "HggHZZHWWHttHbbVBFHttBoost": smH_PTH_HggHZZHWWHttHbbVBFHttBoost,
        "HggHZZHWWHttHbbVBFHttBoost_statonly": smH_PTH_HggHZZHWWHttHbbVBFHttBoost_statonly,
        "HggHZZHWWHttHbbVBFHttBoost_asimov": smH_PTH_HggHZZHWWHttHbbVBFHttBoost_asimov,
        "HggHZZinclusive": smH_PTH_HggHZZinclusive,
    },
    "Njets": {
        "Hgg": Njets_Hgg,
        "Hgg_statonly": Njets_Hgg_statonly,
        "HWW": Njets_HWW,
        "HWW_statonly": Njets_HWW_statonly,
        "Htt": Njets_Htt,
        "HggHWW": Njets_HggHWW,
        "HggHWW_statonly": Njets_HggHWW_statonly,
        "HggHWWHtt": Njets_HggHWWHtt,
        "HggHWWHtt_statonly": Njets_HggHWWHtt_statonly,
    },
    "yH": {
        "Hgg": yH_Hgg,
        "HZZ": yH_HZZ,
        "HZZ_statonly": yH_HZZ_statonly,
        "HZZ_asimov": yH_HZZ_asimov,
        "HggHZZ": yH_HggHZZ,
        "HggHZZ_statonly": yH_HggHZZ_statonly,
        "Hgginclusive": yH_Hgginclusive,
        "HZZinclusive": yH_HZZinclusive,
        "HggHZZinclusive": yH_HggHZZinclusive,
        "HggHZZinclusive_statonly": yH_HggHZZinclusive_statonly,
        "HggHZZinclusive_asimov": yH_HggHZZinclusive_asimov,
    },
    "smH_PTJ0": {
        "Hgg": smH_PTJ0_Hgg,
        "Hgg_statonly": smH_PTJ0_Hgg_statonly,
        "Hgg_asimov": smH_PTJ0_Hgg_asimov,
        "Hgg_asimov_statonly": smH_PTJ0_Hgg_asimov_statonly,
    },
}

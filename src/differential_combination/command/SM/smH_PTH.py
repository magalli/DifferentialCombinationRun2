from ..command import Command, Routine

all_pois_Hgg = [
    "r_smH_PTH_0_5",
    "r_smH_PTH_5_10",
    "r_smH_PTH_10_15",
    "r_smH_PTH_15_20",
    "r_smH_PTH_20_25",
    "r_smH_PTH_25_30",
    "r_smH_PTH_30_35",
    "r_smH_PTH_35_45",
    "r_smH_PTH_45_60",
    "r_smH_PTH_60_80",
    "r_smH_PTH_80_100",
    "r_smH_PTH_100_120",
    "r_smH_PTH_120_140",
    "r_smH_PTH_140_170",
    "r_smH_PTH_170_200",
    "r_smH_PTH_200_250",
    "r_smH_PTH_250_350",
    "r_smH_PTH_350_450",
    "r_smH_PTH_GT450",
]

all_pois_HZZ = [
    "r_smH_PTH_0_10",
    "r_smH_PTH_10_20",
    "r_smH_PTH_20_30",
    "r_smH_PTH_30_45",
    "r_smH_PTH_45_60",
    "r_smH_PTH_60_80",
    "r_smH_PTH_80_120",
    "r_smH_PTH_120_200",
    "r_smH_PTH_GT200",
]

all_pois_HWW = [
    "r_smH_PTH_0_30",
    "r_smH_PTH_30_45",
    "r_smH_PTH_45_80",
    "r_smH_PTH_80_120",
    "r_smH_PTH_120_200",
    "r_smH_PTH_GT200",
]

all_pois_Htt = [
    "r_smH_PTH_0_45",
    "r_smH_PTH_45_80",
    "r_smH_PTH_80_120",
    "r_smH_PTH_120_140",
    "r_smH_PTH_140_170",
    "r_smH_PTH_170_200",
    "r_smH_PTH_200_350",
    "r_smH_PTH_350_450",
    "r_smH_PTH_GT450",
]

all_pois_HggHZZHWWHttHbb = [
    "r_smH_PTH_0_5",
    "r_smH_PTH_5_10",
    "r_smH_PTH_10_15",
    "r_smH_PTH_15_20",
    "r_smH_PTH_20_25",
    "r_smH_PTH_25_30",
    "r_smH_PTH_30_35",
    "r_smH_PTH_35_45",
    "r_smH_PTH_45_60",
    "r_smH_PTH_60_80",
    "r_smH_PTH_80_100",
    "r_smH_PTH_100_120",
    "r_smH_PTH_120_140",
    "r_smH_PTH_140_170",
    "r_smH_PTH_170_200",
    "r_smH_PTH_200_250",
    "r_smH_PTH_250_350",
    "r_smH_PTH_350_450",
    "r_smH_PTH_450_650",
    "r_smH_PTH_GT650",
]

all_pois_HggHZZHWWHttHbbVBF = [
    "r_smH_PTH_0_5",
    "r_smH_PTH_5_10",
    "r_smH_PTH_10_15",
    "r_smH_PTH_15_20",
    "r_smH_PTH_20_25",
    "r_smH_PTH_25_30",
    "r_smH_PTH_30_35",
    "r_smH_PTH_35_45",
    "r_smH_PTH_45_60",
    "r_smH_PTH_60_80",
    "r_smH_PTH_80_100",
    "r_smH_PTH_100_120",
    "r_smH_PTH_120_140",
    "r_smH_PTH_140_170",
    "r_smH_PTH_170_200",
    "r_smH_PTH_200_250",
    "r_smH_PTH_250_350",
    "r_smH_PTH_350_450",
    "r_smH_PTH_450_500",
    "r_smH_PTH_500_550",
    "r_smH_PTH_550_600",
    "r_smH_PTH_600_675",
    "r_smH_PTH_675_800",
    "r_smH_PTH_800_1200",
]

all_pois_HbbVBF = [
    "r_smH_PTH_450_500",
    "r_smH_PTH_500_550",
    "r_smH_PTH_550_600",
    "r_smH_PTH_600_675",
    "r_smH_PTH_675_800",
    "r_smH_PTH_800_1200",
]

all_pois_HttBoost = ["r_smH_PTH_450_600", "r_smH_PTH_GT600"]


acm_string = "--noMCbonly 1 --X-rtd FAST_VERTICAL_MORPH --X-rtd MINIMIZER_multiMin_hideConstants --X-rtd MINIMIZER_multiMin_maskConstraints --X-rtd MINIMIZER_multiMin_maskChannels=2 --X-rtd NO_INITIAL_SNAP --X-rtd SIMNLL_GROUPCONSTRAINTS=10 --X-rtd CACHINGPDF_NOCLONE"


class AsimovWithEnvelope(Routine):
    all_pois = all_pois_Hgg
    task_name = "asimov_with_envelope"
    n_points = 30
    split_points = 5

    def __init__(self, input_dir, pois, name, global_fit_file=None):
        self.commands = []
        self.name = name
        if global_fit_file is None:
            generate_first = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M GenerateOnly",
                    "-m 125.38",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in self.all_pois])
                    ),
                    "--name AsimovPreFit",
                    "--saveToys",
                    "-t -1",
                ],
            )
            self.commands.append(generate_first)
            fit_first = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "-t -1",
                    "--toysFile higgsCombineAsimovPreFit.GenerateOnly.mH125.38.123456.root",
                    "--saveWorkspace",
                    "-n AsimovBestFit",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in self.all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(self.all_pois)),
                    "--saveFitResult",
                    acm_string,
                ],
            )
            self.commands.append(fit_first)
            generate_second = Command(
                executable="combine",
                input_file="higgsCombineAsimovBestFit.MultiDimFit.mH125.38.root",
                args=[
                    "-M GenerateOnly",
                    "-m 125.38",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in self.all_pois])
                    ),
                    "--name AsimovPostFit",
                    "--saveToys",
                    "--saveWorkspace",
                    "--snapshotName MultiDimFit",
                    "-t -1",
                ],
            )
            self.commands.append(generate_second)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    "-t -1",
                    "--toysFile higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                    "--snapshotName MultiDimFit",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(self.all_pois)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points {}".format(self.n_points),
                    "--split-points {}".format(self.split_points),
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.task_name),
                    "-v -1",
                    acm_string,
                ],
            )
            self.commands.append(poi_command)


class Hgg(Routine):
    all_pois = all_pois_Hgg
    task_name = "Hgg"

    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Hgg"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in self.all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(self.all_pois)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                    "--saveFitResult",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    "--snapshotName MultiDimFit",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(self.all_pois)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.task_name),
                    acm_string,
                ],
            )
            self.commands.append(poi_command)


class Hgg_statonly(Hgg):
    task_name = "Hgg_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class Hgg_asimov(AsimovWithEnvelope):
    task_name = "Hgg_asimov"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov, self).__init__(input_dir, pois, name="Hgg")


class HZZ(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HZZ.root".format(input_dir),
                args=[
                    "--name _POSTFIT_HZZ",
                    "-M MultiDimFit",
                    "--algo=none",
                    "--cminDefaultMinimizerStrategy 0",
                    "--freezeParameters MH",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--saveFitResult",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="higgsCombine_POSTFIT_HZZ.MultiDimFit.mH125.38.root",
                args=[
                    "--name _SCAN_{}_HZZ".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--freezeParameters MH",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 30",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HZZ_statonly(HZZ):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HZZ_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option(
                "--freezeParameters MH,allConstrainedNuisances"
            )


class HZZ_asimov(HZZ):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HZZ_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HZZ_asimov_statonly(HZZ_asimov):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HZZ_asimov_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option(
                "--freezeParameters MH,allConstrainedNuisances"
            )


class HWW(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.decay_channel = "HWW"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.decay_channel),
                args=[
                    "--name _POSTFIT_{}".format(self.decay_channel),
                    "-m 125.38",
                    "-M MultiDimFit",
                    "--algo=singles",
                    "--X-rtd MINIMIZER_analytic",
                    "--saveWorkspace",
                    "--cminDefaultMinimizerStrategy 0",
                    "--saveFitResult",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.decay_channel
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.decay_channel),
                    "-m 125.38",
                    "-M MultiDimFit",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--cminDefaultMinimizerStrategy 0",
                    "--X-rtd MINIMIZER_analytic",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HWW])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 10",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G --partition=short"',
                    "--task-name _SCAN_{}_{}".format(
                        poi, self.decay_channel
                    ),  # change me
                ],
            )
            self.commands.append(poi_command)


class HWW_statonly(HWW):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HWW_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HWW_asimov(HWW):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HWW_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class Htt(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Htt"
        global_fit_command = Command(
            executable="combine",
            input_file="{}/{}.root".format(input_dir, self.name),
            args=[
                "--name _POSTFIT_{}".format(self.name),
                "-M MultiDimFit",
                "-m 125",
                "--robustFit=1",
                "--X-rtd MINIMIZER_analytic",
                "--algo=singles",
                "--cl=0.68",
                "--setParameters r_smH_PTH_0_45=0,r_smH_PTH_45_80=0,r_smH_PTH_80_120=1,r_smH_PTH_120_140=0,r_smH_PTH_140_170=1,r_smH_PTH_170_200=1,r_smH_PTH_200_350=1.5,r_smH_PTH_350_450=0.5,r_smH_PTH_GT450=1",
                "--setParameterRanges r_smH_PTH_0_45=-5,5:r_smH_PTH_45_80=-5,5:r_smH_PTH_80_120=-5,5:r_smH_PTH_200_350=-5,5:r_smH_PTH_350_450=-5,5:r_smH_PTH_GT450=-5,5",
                "--redefineSignalPOIs {}".format(",".join(all_pois_Htt)),
                "--floatOtherPOIs=1",
                "--cminDefaultMinimizerStrategy=0",
                "--saveFitResult",
            ],
        )
        self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "--name _SINGLES_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125",
                    "--robustFit=1",
                    "--X-rtd MINIMIZER_analytic",
                    "--algo=singles",
                    "--cl=0.68",
                    "--setParameters r_smH_PTH_0_45=0,r_smH_PTH_45_80=0,r_smH_PTH_80_120=1,r_smH_PTH_120_140=0,r_smH_PTH_140_170=1,r_smH_PTH_170_200=1,r_smH_PTH_200_350=1.5,r_smH_PTH_350_450=0.5,r_smH_PTH_GT450=1",
                    "--setParameterRanges r_smH_PTH_0_45=-5,5:r_smH_PTH_45_80=-5,5:r_smH_PTH_80_120=-5,5:r_smH_PTH_200_350=-5,5:r_smH_PTH_350_450=-5,5:r_smH_PTH_GT450=-5,5",
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Htt)),
                    "--floatOtherPOIs=1",
                    "--cminDefaultMinimizerStrategy=0",
                    "-P {}".format(poi),
                    "--job-mode slurm",
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class Htt_statonly(Htt):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Htt_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class Htt_asimov(Htt):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Htt_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option(
                "--setParameters r_smH_PTH_0_45=1,r_smH_PTH_45_80=1,r_smH_PTH_80_120=1,r_smH_PTH_120_140=1,r_smH_PTH_140_170=1,r_smH_PTH_170_200=1,r_smH_PTH_200_350=1,r_smH_PTH_350_450=1,r_smH_PTH_GT450=1"
            )
            command.add_or_replace_option("--toys -1")


class Hbb(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Hbb"
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "--name _SINGLES_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--setRobustFitTolerance 0.01",
                    "--setRobustFitStrategy 2",
                    "--algo singles",
                    "--robustFit 1",
                    "--setRobustFitAlgo Minuit2,Migrad",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs r_smH_PTH_300_450,r_smH_PTH_450_650,r_smH_PTH_GT650",
                    "--freezeParameters r_smH_PTH_0_200,r_smH_PTH_200_300,r_xHbb",
                    "-P {}".format(poi),
                    "> log_{}.out &".format(poi),
                ],
            )
            self.commands.append(poi_command)


class Hbb_statonly(Hbb):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hbb_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option(
                "--freezeParameters allConstrainedNuisances,r_smH_PTH_0_200,r_smH_PTH_200_300,r_xHbb"
            )


class Hbb_asimov(Hbb):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hbb_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class Hbb_asimov_statonly(Hbb_asimov):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hbb_asimov_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option(
                "--freezeParameters allConstrainedNuisances,r_smH_PTH_0_200,r_smH_PTH_200_300,r_xHbb"
            )


class HbbVBF(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HbbVBF"
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "--name _SINGLES_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo singles",
                    "--robustFit 1",
                    # "--robustHesse 1",
                    "--cminDefaultMinimizerStrategy=0",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HbbVBF])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_HbbVBF)),
                    "-P {}".format(poi),
                    "--job-mode slurm",
                    # "> log_{}.out &".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HbbVBF_statonly(HbbVBF):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HbbVBF_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HbbVBF_asimov(HbbVBF):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HbbVBF_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HttBoost(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HttBoost.root".format(input_dir),
                args=[
                    "--name _POSTFIT_HttBoost",
                    "-M MultiDimFit",
                    "--algo=none",
                    "--freezeParameters MH",
                    "-m 125",
                    "--floatOtherPOIs=1",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HttBoost])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_HttBoost)),
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="higgsCombine_POSTFIT_HttBoost.MultiDimFit.mH125.root",
                args=[
                    "--name _SCAN_{}_HttBoost".format(poi),
                    "-m 125",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--floatOtherPOIs=1",
                    "--saveWorkspace",
                    "--method MultiDimFit",
                    "--points 30",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HttBoost_statonly(HttBoost):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HttBoost_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HttBoost_asimov(HttBoost):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HttBoost_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HggHZZ(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HggHZZ.root".format(input_dir),  # change me
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_HggHZZ",  # change me
                    "--setParameters r_smH_PTH_0_5=1,r_smH_PTH_5_10=1,r_smH_PTH_10_15=1,r_smH_PTH_15_20=1,r_smH_PTH_20_25=1,r_smH_PTH_25_30=1,r_smH_PTH_30_35=1,r_smH_PTH_35_45=1,r_smH_PTH_45_60=1,r_smH_PTH_60_80=1,r_smH_PTH_80_100=1,r_smH_PTH_100_120=1,r_smH_PTH_120_140=1,r_smH_PTH_140_170=1,r_smH_PTH_170_200=1,r_smH_PTH_200_250=1,r_smH_PTH_250_350=1,r_smH_PTH_350_450=1,r_smH_PTH_GT450=1",
                    "--redefineSignalPOIs r_smH_PTH_0_5,r_smH_PTH_5_10,r_smH_PTH_10_15,r_smH_PTH_15_20,r_smH_PTH_20_25,r_smH_PTH_25_30,r_smH_PTH_30_35,r_smH_PTH_35_45,r_smH_PTH_45_60,r_smH_PTH_60_80,r_smH_PTH_80_100,r_smH_PTH_100_120,r_smH_PTH_120_140,r_smH_PTH_140_170,r_smH_PTH_170_200,r_smH_PTH_200_250,r_smH_PTH_250_350,r_smH_PTH_350_450,r_smH_PTH_GT450",
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_HggHZZ.MultiDimFit.mH125.38.root",  # change me
                args=[
                    "--name _SCAN_{}_HggHZZ".format(poi),  # change me
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo=grid",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs r_smH_PTH_0_5,r_smH_PTH_5_10,r_smH_PTH_10_15,r_smH_PTH_15_20,r_smH_PTH_20_25,r_smH_PTH_25_30,r_smH_PTH_30_35,r_smH_PTH_35_45,r_smH_PTH_45_60,r_smH_PTH_60_80,r_smH_PTH_80_100,r_smH_PTH_100_120,r_smH_PTH_120_140,r_smH_PTH_140_170,r_smH_PTH_170_200,r_smH_PTH_200_250,r_smH_PTH_250_350,r_smH_PTH_350_450,r_smH_PTH_GT450",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G --partition=long --time=10:00:00"',
                    "--task-name _SCAN_{}_HggHZZ".format(poi),  # change me
                ],
            )
            self.commands.append(poi_command)


class HggHZZ_statonly(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HggHZZ.root".format(input_dir),  # change me
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_HggHZZ",  # change me
                    "--setParameters r_smH_PTH_0_5=1,r_smH_PTH_5_10=1,r_smH_PTH_10_15=1,r_smH_PTH_15_20=1,r_smH_PTH_20_25=1,r_smH_PTH_25_30=1,r_smH_PTH_30_35=1,r_smH_PTH_35_45=1,r_smH_PTH_45_60=1,r_smH_PTH_60_80=1,r_smH_PTH_80_100=1,r_smH_PTH_100_120=1,r_smH_PTH_120_140=1,r_smH_PTH_140_170=1,r_smH_PTH_170_200=1,r_smH_PTH_200_250=1,r_smH_PTH_250_350=1,r_smH_PTH_350_450=1,r_smH_PTH_GT450=1",
                    "--redefineSignalPOIs r_smH_PTH_0_5,r_smH_PTH_5_10,r_smH_PTH_10_15,r_smH_PTH_15_20,r_smH_PTH_20_25,r_smH_PTH_25_30,r_smH_PTH_30_35,r_smH_PTH_35_45,r_smH_PTH_45_60,r_smH_PTH_60_80,r_smH_PTH_80_100,r_smH_PTH_100_120,r_smH_PTH_120_140,r_smH_PTH_140_170,r_smH_PTH_170_200,r_smH_PTH_200_250,r_smH_PTH_250_350,r_smH_PTH_350_450,r_smH_PTH_GT450",
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_HggHZZ.MultiDimFit.mH125.38.root",  # change me
                args=[
                    "--name _SCAN_{}_HggHZZ".format(poi),  # change me
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--algo=grid",
                    "--cminDefaultMinimizerAlgo Migrad",
                    "--cminDefaultMinimizerStrategy 0",
                    "--cminDefaultMinimizerTolerance 0.1",
                    "--cminDefaultMinimizerType Minuit2",
                    "--floatOtherPOIs=1",
                    "--freezeParameters allConstrainedNuisances",  # defines statonly
                    "--redefineSignalPOIs r_smH_PTH_0_5,r_smH_PTH_5_10,r_smH_PTH_10_15,r_smH_PTH_15_20,r_smH_PTH_20_25,r_smH_PTH_25_30,r_smH_PTH_30_35,r_smH_PTH_35_45,r_smH_PTH_45_60,r_smH_PTH_60_80,r_smH_PTH_80_100,r_smH_PTH_100_120,r_smH_PTH_120_140,r_smH_PTH_140_170,r_smH_PTH_170_200,r_smH_PTH_200_250,r_smH_PTH_250_350,r_smH_PTH_350_450,r_smH_PTH_GT450",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G --partition=long --time=20:00:00"',
                    "--task-name _SCAN_{}_HggHZZ".format(poi),  # change me
                ],
            )
            self.commands.append(poi_command)


class HggHZZ_asimov(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HggHZZ.root".format(input_dir),  # change me
                args=[
                    "-M MultiDimFit",
                    "-t -1",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_HggHZZ_asimov",  # change me
                    "--setParameters r_smH_PTH_0_5=1,r_smH_PTH_5_10=1,r_smH_PTH_10_15=1,r_smH_PTH_15_20=1,r_smH_PTH_20_25=1,r_smH_PTH_25_30=1,r_smH_PTH_30_35=1,r_smH_PTH_35_45=1,r_smH_PTH_45_60=1,r_smH_PTH_60_80=1,r_smH_PTH_80_100=1,r_smH_PTH_100_120=1,r_smH_PTH_120_140=1,r_smH_PTH_140_170=1,r_smH_PTH_170_200=1,r_smH_PTH_200_250=1,r_smH_PTH_250_350=1,r_smH_PTH_350_450=1,r_smH_PTH_GT450=1",
                    "--redefineSignalPOIs r_smH_PTH_0_5,r_smH_PTH_5_10,r_smH_PTH_10_15,r_smH_PTH_15_20,r_smH_PTH_20_25,r_smH_PTH_25_30,r_smH_PTH_30_35,r_smH_PTH_35_45,r_smH_PTH_45_60,r_smH_PTH_60_80,r_smH_PTH_80_100,r_smH_PTH_100_120,r_smH_PTH_120_140,r_smH_PTH_140_170,r_smH_PTH_170_200,r_smH_PTH_200_250,r_smH_PTH_250_350,r_smH_PTH_350_450,r_smH_PTH_GT450",
                    # Thomas options
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # end Thomas options
                    # ACM options
                    # "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    # "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    # "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_HggHZZ_asimov.MultiDimFit.mH125.38.root",  # change me
                args=[
                    "--name _SCAN_{}_HggHZZ_asimov".format(poi),  # change me
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo=grid",
                    "-t -1",
                    # ACM options
                    # "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    # "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    # "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    # Thomas options
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # end Thomas options
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs r_smH_PTH_0_5,r_smH_PTH_5_10,r_smH_PTH_10_15,r_smH_PTH_15_20,r_smH_PTH_20_25,r_smH_PTH_25_30,r_smH_PTH_30_35,r_smH_PTH_35_45,r_smH_PTH_45_60,r_smH_PTH_60_80,r_smH_PTH_80_100,r_smH_PTH_100_120,r_smH_PTH_120_140,r_smH_PTH_140_170,r_smH_PTH_170_200,r_smH_PTH_200_250,r_smH_PTH_250_350,r_smH_PTH_350_450,r_smH_PTH_GT450",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 1",
                    "--job-mode slurm",
                    #'--sub-opts="--mem=5G --partition=long --time=10:00:00"',
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_HggHZZ_asimov".format(poi),  # change me
                ],
            )
            self.commands.append(poi_command)


class HggHZZHWW(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHZZHWW"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_Hgg])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Hgg)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Hgg)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    #'--sub-opts="--mem=5G --partition=long --time=3-00:00:00"',
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class HggHZZHWW_statonly(HggHZZHWW):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWW_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZHWW_asimov(HggHZZHWW):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWW_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HggHZZHWWHtt(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHZZHWWHtt"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_Hgg])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Hgg)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Hgg)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    #'--sub-opts="--mem=5G --partition=long --time=3-00:00:00"',
                    '--sub-opts="--mem=15G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                    acm_string,
                ],
            )
            self.commands.append(poi_command)


class HggHZZHWWHtt_statonly(HggHZZHWWHtt):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHtt_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZHWWHtt_asimov(AsimovWithEnvelope):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHtt_asimov, self).__init__(input_dir, pois, name="HggHZZHWWHtt")


class HggHZZHWWHttHbb(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHZZHWWHttHbb"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HggHZZHWWHttHbb])
                    ),
                    "--redefineSignalPOIs {}".format(
                        ",".join(all_pois_HggHZZHWWHttHbb)
                    ),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(
                        ",".join(all_pois_HggHZZHWWHttHbb)
                    ),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=15G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                    '--setParameterRanges "r_smH_PTH_GT650=-5,40"',
                    "-v -1",
                    acm_string,
                ],
            )
            self.commands.append(poi_command)


class HggHZZHWWHttHbb_statonly(HggHZZHWWHttHbb):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbb_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZHWWHttHbb_asimov(AsimovWithEnvelope):
    all_pois = all_pois_HggHZZHWWHttHbb
    task_name = "HggHZZHWWHttHbb_asimov"
    n_points = 30
    split_points = 1

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbb_asimov, self).__init__(
            input_dir, pois, name="HggHZZHWWHttHbb", global_fit_file=global_fit_file
        )


class HggHZZHWWHttHbb_asimov_statonly(HggHZZHWWHttHbb_asimov):
    task_name = "HggHZZHWWHttHbb_asimov_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbb_asimov_statonly, self).__init__(
            input_dir, pois, global_fit_file
        )
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZHWWHttHbbVBF(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHZZHWWHttHbbVBF"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(
                            ["{}=1".format(p) for p in all_pois_HggHZZHWWHttHbbVBF]
                        )
                    ),
                    "--redefineSignalPOIs {}".format(
                        ",".join(all_pois_HggHZZHWWHttHbbVBF)
                    ),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(
                        ",".join(all_pois_HggHZZHWWHttHbbVBF)
                    ),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=15G"',
                    '--setParameterRanges "r_smH_PTH_450_500=-3,6:r_smH_PTH_500_550=-5,8:r_smH_PTH_550_600=-3,14:r_smH_PTH_600_675=-4,14:r_smH_PTH_675_800=-20,10:r_smH_PTH_800_1200=-25,6"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                    "-v -1",
                    acm_string,
                ],
            )
            self.commands.append(poi_command)


class HggHZZHWWHttHbbVBF_statonly(HggHZZHWWHttHbbVBF):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbbVBF_statonly, self).__init__(
            input_dir, pois, global_fit_file
        )
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZHWWHttHbbVBF_asimov(AsimovWithEnvelope):
    all_pois = all_pois_HggHZZHWWHttHbbVBF
    task_name = "HggHZZHWWHttHbbVBF_asimov"
    n_points = 30
    split_points = 1

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbbVBF_asimov, self).__init__(
            input_dir, pois, name="HggHZZHWWHttHbbVBF", global_fit_file=global_fit_file
        )


class HggHZZHWWHttHbbVBFHttBoost(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHZZHWWHttHbbVBFHttBoost"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(
                            ["{}=1".format(p) for p in all_pois_HggHZZHWWHttHbbVBF]
                        )
                    ),
                    "--redefineSignalPOIs {}".format(
                        ",".join(all_pois_HggHZZHWWHttHbbVBF)
                    ),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(
                        ",".join(all_pois_HggHZZHWWHttHbbVBF)
                    ),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=15G"',
                    '--setParameterRanges "r_smH_PTH_450_500=-3,6:r_smH_PTH_500_550=-5,8:r_smH_PTH_550_600=-3,14:r_smH_PTH_600_675=-4,14:r_smH_PTH_675_800=-20,10:r_smH_PTH_800_1200=-25,6"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                    "-v -1",
                    acm_string,
                ],
            )
            self.commands.append(poi_command)


class HggHZZHWWHttHbbVBFHttBoost_statonly(HggHZZHWWHttHbbVBFHttBoost):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbbVBFHttBoost_statonly, self).__init__(
            input_dir, pois, global_fit_file
        )
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZHWWHttHbbVBFHttBoost_asimov(AsimovWithEnvelope):
    all_pois = all_pois_HggHZZHWWHttHbbVBF
    task_name = "HggHZZHWWHttHbbVBFHttBoost_asimov"
    n_points = 30
    split_points = 1

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHWWHttHbbVBFHttBoost_asimov, self).__init__(
            input_dir,
            pois,
            name="HggHZZHWWHttHbbVBFHttBoost",
            global_fit_file=global_fit_file,
        )


# All inclusive
class HggHZZinclusive(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HggHZZinclusive.root".format(input_dir),  # change me
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_HggHZZinclusive",  # change me
                    "--setParameters r_smH_PTH_inclusive=1",
                    "--redefineSignalPOIs r_smH_PTH_inclusive",
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_HggHZZinclusive.MultiDimFit.mH125.38.root",  # change me
                args=[
                    "--name _SCAN_{}_HggHZZinclusive".format(poi),  # change me
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo=grid",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs r_smH_PTH_inclusive",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_HggHZZinclusive".format(poi),  # change me
                ],
            )
            self.commands.append(poi_command)

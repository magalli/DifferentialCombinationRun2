from ..command import Command, Routine

all_pois = ["r_Njets_0", "r_Njets_1", "r_Njets_2", "r_Njets_3", "r_Njets_4"]


class Hgg(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Hgg"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # ACM options
                    # "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    # "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    # "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    # ACM options
                    # "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    # "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    # "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class Hgg_statonly(Hgg):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class Hgg_asimov(Hgg):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HWW(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.decay_channel = "HWW"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.decay_channel),
                args=[
                    "--name _POSTFIT_{}".format(self.decay_channel),
                    "-m 125.38",
                    "-M MultiDimFit",
                    "--algo=singles",
                    "--X-rtd MINIMIZER_analytic",
                    "--saveWorkspace",
                    "--cminDefaultMinimizerStrategy 0",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.decay_channel
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.decay_channel),
                    "-m 125.38",
                    "-M MultiDimFit",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--cminDefaultMinimizerStrategy 0",
                    "--X-rtd MINIMIZER_analytic",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    # "--rMax 4",
                    "--rMax 8",
                    "--split-points 10",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G --partition=short"',
                    "--task-name _SCAN_{}_{}".format(
                        poi, self.decay_channel
                    ),  # change me
                ],
            )
            self.commands.append(poi_command)


class HWW_statonly(HWW):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HWW_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class Htt(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Htt"
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "--name _SINGLES_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125",
                    "--robustFit=1",
                    "--X-rtd MINIMIZER_analytic",
                    "--algo=singles",
                    "--cl=0.68",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--floatOtherPOIs=1",
                    "--cminDefaultMinimizerStrategy=0",
                    "-P {}".format(poi),
                    "> log_{}.out &".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HggHWW(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHWW"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # ACM options
                    # "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    # "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    # "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    # ACM options
                    # "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    # "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    # "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--rMin -2",
                    "--rMax 4",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class HggHWW_statonly(HggHWW):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHWW_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHWWHtt(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHWWHtt"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 40",
                    "--rMin -2",
                    "--rMax 4",
                    "--split-points 4",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class HggHWWHtt_statonly(HggHWWHtt):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHWWHtt_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")

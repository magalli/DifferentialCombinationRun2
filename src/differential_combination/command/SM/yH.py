from ..command import Command, Routine


all_pois_Hgg = [
    "r_yH_0p0_0p15",
    "r_yH_0p15_0p3",
    "r_yH_0p3_0p45",
    "r_yH_0p45_0p6",
    "r_yH_0p6_0p75",
    "r_yH_0p75_0p9",
    "r_yH_0p9_1p2",
    "r_yH_1p2_1p6",
    "r_yH_1p6_2p0",
    "r_yH_2p0_3p0",
]

all_pois_HZZ = [
    "r_yH_0p0_0p15",
    "r_yH_0p15_0p3",
    "r_yH_0p3_0p45",
    "r_yH_0p45_0p6",
    "r_yH_0p6_0p75",
    "r_yH_0p75_0p9",
    "r_yH_0p9_1p2",
    "r_yH_1p2_1p6",
    "r_yH_1p6_3p0",
]

all_pois_HggHZZ = [
    "r_yH_0p0_0p15",
    "r_yH_0p15_0p3",
    "r_yH_0p3_0p45",
    "r_yH_0p45_0p6",
    "r_yH_0p6_0p75",
    "r_yH_0p75_0p9",
    "r_yH_0p9_1p2",
    "r_yH_1p2_1p6",
    "r_yH_1p6_2p0",
    "r_yH_2p0_3p0",
]


class Hgg(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Hgg"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_Hgg])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Hgg)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Hgg)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 30",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G --time=04:00:00"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class Hgg_statonly(Hgg):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HZZ(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HZZ.root".format(input_dir),
                args=[
                    "--name _POSTFIT_HZZ",
                    "-M MultiDimFit",
                    "--algo=none",
                    "--cminDefaultMinimizerStrategy 0",
                    "--freezeParameters MH",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="higgsCombine_POSTFIT_HZZ.MultiDimFit.mH125.38.root",
                args=[
                    "--name _SCAN_{}_HZZ".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--freezeParameters MH",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 60",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HZZ_statonly(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HZZ.root".format(input_dir),
                args=[
                    "--name _POSTFIT_HZZ",
                    "-M MultiDimFit",
                    "--algo=none",
                    "--cminDefaultMinimizerStrategy 0",
                    "--freezeParameters MH",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="higgsCombine_POSTFIT_HZZ.MultiDimFit.mH125.38.root",
                args=[
                    "--name _SCAN_{}_HZZ".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--freezeParameters allConstrainedNuisances",  # defines statonly
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 30",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HZZ_asimov(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HZZ.root".format(input_dir),
                args=[
                    "--name _POSTFIT_HZZ",
                    "-M MultiDimFit",
                    "--algo=none",
                    "--cminDefaultMinimizerStrategy 0",
                    "--freezeParameters MH",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "--toys -1",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="higgsCombine_POSTFIT_HZZ.MultiDimFit.mH125.38.root",
                args=[
                    "--name _SCAN_{}_HZZ".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--freezeParameters MH",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 30",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "--toys -1",
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HggHZZ(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "HggHZZ"
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HggHZZ])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_HggHZZ)),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    # ACM options
                    "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    self.name
                ),
                args=[
                    "--name _SCAN_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo grid",
                    # ACM options
                    "--noMCbonly 1",
                    # "--cminApproxPreFitTolerance=100",
                    # "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    # "--X-rtd MINIMIZER_MaxCalls=9999999",
                    # "--X-rtd MINIMIZER_analytic",
                    # "--X-rtd FAST_VERTICAL_MORPH",
                    # "--X-rtd MINIMIZER_multiMin_hideConstants",
                    # "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    # "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    # "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    # "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    '--snapshotName "MultiDimFit"',
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs {}".format(",".join(all_pois_HggHZZ)),
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 50",
                    "--split-points 1",
                    # "--rMin -2",
                    "--rMin -4",
                    # "--rMax 2",
                    "--rMax 4",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class HggHZZ_statonly(HggHZZ):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZ_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


# Inclusive


class Hgginclusive(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/Hgginclusive.root".format(input_dir),  # change me
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_Hgginclusive",  # change me
                    "--setParameters r_yH_inclusive=1",
                    "--redefineSignalPOIs r_yH_inclusive",
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_Hgginclusive.MultiDimFit.mH125.38.root",  # change me
                args=[
                    "--name _SCAN_{}_Hgginclusive".format(poi),  # change me
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo=grid",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs r_yH_inclusive",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_Hgginclusive".format(poi),  # change me
                ],
            )
            self.commands.append(poi_command)


class HZZinclusive(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HZZinclusive.root".format(input_dir),
                args=[
                    "--name _POSTFIT_HZZinclusive",
                    "-M MultiDimFit",
                    "--algo=none",
                    "--cminDefaultMinimizerStrategy 0",
                    "--freezeParameters MH",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--floatOtherPOIs=1",
                    "--setParameters r_yH_inclusive=1",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="higgsCombine_POSTFIT_HZZinclusive.MultiDimFit.mH125.38.root",
                args=[
                    "--name _SCAN_{}_HZZinclusive".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--snapshotName MultiDimFit",
                    "--freezeParameters MH",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 60",
                    "--saveWorkspace",
                    "--setParameters r_yH_inclusive=1",
                    "-P r_yH_inclusive",
                    "--redefineSignalPOIs r_yH_inclusive",
                ],
            )
            self.commands.append(poi_command)


class HggHZZinclusive(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/HggHZZinclusive.root".format(input_dir),  # change me
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_HggHZZinclusive",  # change me
                    "--setParameters r_yH_inclusive=1",
                    "--redefineSignalPOIs r_yH_inclusive",
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                ],
            )
            self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_HggHZZinclusive.MultiDimFit.mH125.38.root",  # change me
                args=[
                    "--name _SCAN_{}_HggHZZinclusive".format(poi),  # change me
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--algo=grid",
                    # ACM options
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                    # end ACM options
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs r_yH_inclusive",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--rMin -1",
                    "--rMax 3",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=5G"',
                    "--task-name _SCAN_{}_HggHZZinclusive".format(poi),  # change me
                ],
            )
            self.commands.append(poi_command)


class HggHZZinclusive_statonly(HggHZZinclusive):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZinclusive_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HggHZZinclusive_asimov(HggHZZinclusive):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZinclusive_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")

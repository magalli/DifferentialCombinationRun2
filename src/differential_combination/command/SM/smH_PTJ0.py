from ..command import Command, Routine
from smH_PTH import AsimovWithEnvelope, acm_string
from smH_PTH import Hgg as smH_PTH_Hgg

all_pois_Hgg = [
    "r_smH_PTJ0_0_30",
    "r_smH_PTJ0_30_40",
    "r_smH_PTJ0_40_55",
    "r_smH_PTJ0_55_75",
    "r_smH_PTJ0_75_95",
    "r_smH_PTJ0_95_120",
    "r_smH_PTJ0_120_150",
    "r_smH_PTJ0_150_200",
    "r_smH_PTJ0_GT200",
]


class Hgg_asimov(AsimovWithEnvelope):
    all_pois = all_pois_Hgg
    task_name = "Hgg_asimov"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov, self).__init__(
            input_dir, pois, name="Hgg", global_fit_file=global_fit_file
        )


class Hgg_asimov_statonly(Hgg_asimov):
    task_name = "Hgg_asimov_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov_statonly, self).__init__(
            input_dir, pois, global_fit_file=global_fit_file
        )
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class Hgg(smH_PTH_Hgg):
    all_pois = all_pois_Hgg
    task_name = "Hgg"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg, self).__init__(input_dir, pois, global_fit_file=global_fit_file)


class Hgg_statonly(Hgg):
    task_name = "Hgg_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")

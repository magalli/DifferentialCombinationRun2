from ..command import Command, Routine


class Hgg(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        if global_fit_file is None:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/Hgg.root".format(input_dir),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_Hgg",
                    "--setParameters cdp=1,cpg=1,ctp=1,ctg=1",
                    "--redefineSignalPOIs cdp,cpg,ctp,ctg",
                    "--noMCbonly 1",
                    "--cminApproxPreFitTolerance=100",
                    "--cminFallbackAlgo Minuit2,Migrad,0:0.1",
                    "--X-rtd MINIMIZER_MaxCalls=9999999",
                    "--X-rtd MINIMIZER_analytic",
                    "--X-rtd FAST_VERTICAL_MORPH",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--X-rtd MINIMIZER_multiMin_hideConstants",
                    "--X-rtd MINIMIZER_multiMin_maskConstraints",
                    "--X-rtd MINIMIZER_multiMin_maskChannels=2",
                    "--X-rtd OPTIMIZE_BOUNDS=0",
                    "--X-rtd NO_INITIAL_SNAP",
                    "--X-rtd SIMNLL_GROUPCONSTRAINTS=10",
                    "--X-rtd CACHINGPDF_NOCLONE",
                ],
            )
            self.commands.append(global_fit_command)

        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="higgsCombine_POSTFIT_Hgg.MultiDimFit.mH125.38.root",
                args=[
                    "--name _SCAN_{}_Hgg".format(poi),
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--algo=grid",
                    "--cminDefaultMinimizerAlgo Migrad",
                    "--cminDefaultMinimizerStrategy 0",
                    "--cminDefaultMinimizerTolerance 0.1",
                    "--cminDefaultMinimizerType Minuit2",
                    "--floatOtherPOIs=1",
                    "--redefineSignalPOIs cdp,cpg,ctp,ctg",
                    "--saveInactivePOI 1",
                    "--saveNLL",
                    "--skipInitialFit",
                    "--squareDistPoiStep",
                    "-P {}".format(poi),
                    "--points 100",
                    "--split-points 1",
                    "--job-mode slurm",
                    '--sub-opts="--mem=3G --partition=long --time=10:00:00"',
                    "--task-name _SCAN_{}_Hgg".format(poi),
                ],
            )
            self.commands.append(poi_command)

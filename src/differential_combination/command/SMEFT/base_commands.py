import logging
from copy import deepcopy
from itertools import combinations

logger = logging.getLogger(__name__)

from ..command import Command, Routine


acm_string = "--noMCbonly 1 --X-rtd FAST_VERTICAL_MORPH --X-rtd MINIMIZER_multiMin_hideConstants --X-rtd MINIMIZER_multiMin_maskConstraints --X-rtd MINIMIZER_multiMin_maskChannels=2 --X-rtd NO_INITIAL_SNAP --X-rtd SIMNLL_GROUPCONSTRAINTS=10 --X-rtd CACHINGPDF_NOCLONE"


class AsimovWithEnvelope(Routine):
    def __init__(
        self,
        model_name,
        submodel_name,
        input_dir,
        name,
        main_model_pois,
        pois_to_fit,
        submodel_pois=None,
        skip_2d=False,
        skip_scans=False,
        set_parameter_ranges_string="",
        twod_only=False,
        statonly=False,
        skip_best=False,
        points_one_dim=100,
        split_points_one_dim=5,
        points_two_dim=600,
        split_points_two_dim=4,
        tk=False,
    ):
        self.commands = []
        self.name = name if tk else name.split("_")[0]
        self.task_name = name
        self.main_model_pois = main_model_pois
        self.pois_to_fit = pois_to_fit
        if submodel_pois is None:
            self.submodel_pois = []
        else:
            self.submodel_pois = submodel_pois
        if self.submodel_pois:
            self.pois_to_freeze = [
                p for p in self.main_model_pois if p not in self.submodel_pois
            ]
        else:
            self.pois_to_freeze = [
                p for p in self.main_model_pois if p not in self.pois_to_fit
            ]
        logger.debug("Main model pois: {}".format(self.main_model_pois))
        logger.debug("Submodel pois: {}".format(self.submodel_pois))
        logger.debug("Pois to fit: {}".format(self.pois_to_fit))
        logger.debug("Pois to freeze: {}".format(self.pois_to_freeze))
        if not skip_best:
            generate_first = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M GenerateOnly",
                    "-m 125.38",
                    "--setParameters {}".format(
                        ",".join(["{}=0".format(p) for p in self.main_model_pois])
                    ),
                    "--name AsimovPreFit",
                    "--saveToys",
                    "-t -1",
                    set_parameter_ranges_string,
                ],
            )
            self.commands.append(generate_first)
            fit_first = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "-t -1",
                    "--toysFile higgsCombineAsimovPreFit.GenerateOnly.mH125.38.123456.root",
                    "--saveWorkspace",
                    "-n AsimovBestFit",
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                    "--setParameters {}".format(
                        ",".join(["{}=0".format(p) for p in self.main_model_pois])
                    ),
                    "--redefineSignalPOIs {}".format(
                        ",".join(
                            self.pois_to_fit
                            if not self.submodel_pois
                            else self.submodel_pois
                        )
                    ),
                    set_parameter_ranges_string,
                    # "--saveFitResult --robustHesse 1 --robustFit 1",
                    # "--saveFitResult",
                    # "--robustHesseSave hessian.root",
                ],
            )
            if self.pois_to_freeze:
                fit_first.add_or_replace_option(
                    "--freezeParameters {}".format(",".join(self.pois_to_freeze))
                )
            if statonly:
                fit_first.add_or_replace_option(
                    "--freezeParameters allConstrainedNuisances", force_add=True
                )
            self.commands.append(fit_first)
            generate_second = Command(
                executable="combine",
                input_file="higgsCombineAsimovBestFit.MultiDimFit.mH125.38.root",
                args=[
                    "-M GenerateOnly",
                    "-m 125.38",
                    "--setParameters {}".format(
                        ",".join(["{}=0".format(p) for p in self.main_model_pois])
                    ),
                    "--name AsimovPostFit",
                    "--saveToys",
                    "--saveWorkspace",
                    "--snapshotName MultiDimFit",
                    "-t -1",
                    set_parameter_ranges_string,
                ],
            )
            self.commands.append(generate_second)
        if len(self.pois_to_fit) == 1 and not self.submodel_pois:
            if not skip_scans:
                for poi in self.pois_to_fit:
                    poi_command = Command(
                        executable="combineTool.py",
                        input_file="higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                        args=[
                            "--name _SCAN_{}_{}".format(poi, self.name),
                            "-M MultiDimFit",
                            "-m 125.38",
                            "--algo grid",
                            "-t -1",
                            "--toysFile higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                            acm_string,
                            # end ACM options
                            "--snapshotName MultiDimFit",
                            "--X-rtd MINIMIZER_freezeDisassociatedParams",
                            "--cminDefaultMinimizerStrategy 0",
                            "--floatOtherPOIs=0",
                            "--squareDistPoiStep",
                            "-P {}".format(poi),
                            "--setParameters {}".format(
                                ",".join(
                                    ["{}=0".format(p) for p in self.main_model_pois]
                                )
                            ),
                            "--redefineSignalPOIs {}".format(
                                ",".join(self.pois_to_fit)
                            ),
                            "--points {}".format(points_one_dim),
                            "--split-points {}".format(split_points_one_dim),
                            "--job-mode slurm",
                            '--sub-opts="--mem=15G"',
                            "--task-name _SCAN_{}_{}_{}_{}".format(
                                model_name, submodel_name, poi, self.task_name
                            ),
                            "-v -1",
                            set_parameter_ranges_string,
                        ],
                    )
                    if self.pois_to_freeze:
                        poi_command.add_or_replace_option(
                            "--freezeParameters {}".format(
                                ",".join(self.pois_to_freeze)
                            )
                        )
                    if statonly:
                        poi_command.add_or_replace_option(
                            "--freezeParameters allConstrainedNuisances", force_add=True
                        )
                    self.commands.append(poi_command)
        else:
            if not skip_scans:
                if not twod_only:
                    for poi in self.pois_to_fit:
                        poi_command = Command(
                            executable="combineTool.py",
                            input_file="higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                            args=[
                                "--name _SCAN_1D{}".format(poi),
                                "-M MultiDimFit",
                                "-m 125.38",
                                "--algo grid",
                                "-t -1",
                                "--toysFile higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                                acm_string,
                                # end ACM options
                                "--snapshotName MultiDimFit",
                                "--X-rtd MINIMIZER_freezeDisassociatedParams",
                                "--cminDefaultMinimizerStrategy 0",
                                "--floatOtherPOIs=1",
                                "--squareDistPoiStep",
                                "-P {}".format(poi),
                                "--setParameters {}".format(
                                    ",".join(
                                        ["{}=0".format(p) for p in self.main_model_pois]
                                    )
                                ),
                                "--redefineSignalPOIs {}".format(
                                    ",".join(self.pois_to_fit)
                                ),
                                "--points {}".format(points_one_dim),
                                "--split-points {}".format(split_points_one_dim)
                                if statonly
                                else "--split-points 2",
                                "--job-mode slurm",
                                '--sub-opts="--mem=15G"',
                                "--task-name _SCAN_{}_{}_{}_{}".format(
                                    model_name, submodel_name, poi, self.task_name
                                ),
                                "-v -1",
                                set_parameter_ranges_string,
                            ],
                        )
                        if self.pois_to_freeze:
                            poi_command.add_or_replace_option(
                                "--freezeParameters {}".format(
                                    ",".join(self.pois_to_freeze)
                                )
                            )
                        if statonly:
                            poi_command.add_or_replace_option(
                                "--freezeParameters allConstrainedNuisances",
                                force_add=True,
                            )
                        self.commands.append(poi_command)
                if not skip_2d:
                    for pois in combinations(self.pois_to_fit, 2):
                        poi_command = Command(
                            executable="combineTool.py",
                            input_file="higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                            args=[
                                "--name _SCAN_2D{}".format("-".join(pois)),
                                "-M MultiDimFit",
                                "-m 125.38",
                                "--algo grid",
                                "-t -1",
                                "--toysFile higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                                acm_string,
                                # end ACM options
                                "--snapshotName MultiDimFit",
                                "--X-rtd MINIMIZER_freezeDisassociatedParams",
                                "--cminDefaultMinimizerStrategy 0",
                                "--floatOtherPOIs=1",
                                "--squareDistPoiStep",
                                " ".join(["-P {}".format(p) for p in pois]),
                                "--setParameters {}".format(
                                    ",".join(
                                        ["{}=0".format(p) for p in self.main_model_pois]
                                    )
                                ),
                                "--redefineSignalPOIs {}".format(
                                    ",".join(self.pois_to_fit)
                                ),
                                "--points {}".format(points_two_dim),
                                "--split-points {}".format(split_points_two_dim),
                                "--job-mode slurm",
                                '--sub-opts="--mem=15G"',
                                "--task-name _SCAN_{}_{}_{}_{}".format(
                                    model_name,
                                    submodel_name,
                                    "-".join(pois),
                                    self.task_name,
                                ),
                                "-v -1",
                                set_parameter_ranges_string,
                            ],
                        )
                        if self.pois_to_freeze:
                            poi_command.add_or_replace_option(
                                "--freezeParameters {}".format(
                                    ",".join(self.pois_to_freeze)
                                )
                            )
                        if statonly:
                            poi_command.add_or_replace_option(
                                "--freezeParameters allConstrainedNuisances",
                                force_add=True,
                            )
                        self.commands.append(poi_command)


class Observed(Routine):
    def __init__(
        self,
        model_name,
        submodel_name,
        input_dir,
        name,
        main_model_pois,
        pois_to_fit,
        submodel_pois=None,
        skip_2d=False,
        skip_scans=False,
        set_parameter_ranges_string="",
        twod_only=False,
        statonly=False,
        skip_best=False,
        points_one_dim=100,
        split_points_one_dim=5,
        points_two_dim=600,
        split_points_two_dim=4,
        tk=False,
    ):
        self.commands = []
        self.name = name if tk else name.split("_")[0]
        self.task_name = name
        self.main_model_pois = main_model_pois
        self.pois_to_fit = pois_to_fit
        if submodel_pois is None:
            self.submodel_pois = []
        else:
            self.submodel_pois = submodel_pois
        if self.submodel_pois:
            self.pois_to_freeze = [
                p for p in self.main_model_pois if p not in self.submodel_pois
            ]
        else:
            self.pois_to_freeze = [
                p for p in self.main_model_pois if p not in self.pois_to_fit
            ]
        logger.debug("Main model pois: {}".format(self.main_model_pois))
        logger.debug("Submodel pois: {}".format(self.submodel_pois))
        logger.debug("Pois to fit: {}".format(self.pois_to_fit))
        logger.debug("Pois to freeze: {}".format(self.pois_to_freeze))
        if not skip_best:
            global_fit_command = Command(
                executable="combine",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "-M MultiDimFit",
                    "-m 125.38",
                    "--saveWorkspace",
                    "--name _POSTFIT_{}".format(self.name),
                    "--X-rtd MINIMIZER_freezeDisassociatedParams",
                    "--cminDefaultMinimizerStrategy 0",
                    acm_string,
                    "--setParameters {}".format(
                        ",".join(["{}=0".format(p) for p in self.main_model_pois])
                    ),
                    "--redefineSignalPOIs {}".format(
                        ",".join(
                            self.pois_to_fit
                            if not self.submodel_pois
                            else self.submodel_pois
                        )
                    ),
                    set_parameter_ranges_string,
                    # "--saveFitResult --robustHesse 1 --robustFit 1",
                    # "--saveFitResult",
                    # "--robustHesseSave hessian.root",
                ],
            )
            if self.pois_to_freeze:
                global_fit_command.add_or_replace_option(
                    "--freezeParameters {}".format(",".join(self.pois_to_freeze))
                )
            if statonly:
                global_fit_command.add_or_replace_option(
                    "--freezeParameters allConstrainedNuisances", force_add=True
                )
            self.commands.append(global_fit_command)
        if len(self.pois_to_fit) == 1 and not self.submodel_pois:
            if not skip_scans:
                for poi in self.pois_to_fit:
                    poi_command = Command(
                        executable="combineTool.py",
                        input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                            self.name
                        ),
                        args=[
                            "--name _SCAN_{}_{}".format(poi, self.name),
                            "-M MultiDimFit",
                            "-m 125.38",
                            "--algo grid",
                            acm_string,
                            # end ACM options
                            "--snapshotName MultiDimFit",
                            "--X-rtd MINIMIZER_freezeDisassociatedParams",
                            "--cminDefaultMinimizerStrategy 0",
                            "--floatOtherPOIs=0",
                            "--squareDistPoiStep",
                            "-P {}".format(poi),
                            "--setParameters {}".format(
                                ",".join(
                                    ["{}=0".format(p) for p in self.main_model_pois]
                                )
                            ),
                            "--redefineSignalPOIs {}".format(
                                ",".join(self.pois_to_fit)
                            ),
                            "--points {}".format(points_one_dim),
                            "--split-points {}".format(split_points_one_dim),
                            "--job-mode slurm",
                            '--sub-opts="--mem=15G"',
                            "--task-name _SCAN_{}_{}_{}_{}".format(
                                model_name, submodel_name, poi, self.task_name
                            ),
                            "-v -1",
                            set_parameter_ranges_string,
                        ],
                    )
                    if self.pois_to_freeze:
                        poi_command.add_or_replace_option(
                            "--freezeParameters {}".format(
                                ",".join(self.pois_to_freeze)
                            )
                        )
                    if statonly:
                        poi_command.add_or_replace_option(
                            "--freezeParameters allConstrainedNuisances", force_add=True
                        )
                    self.commands.append(poi_command)
        else:
            if not skip_scans:
                if not twod_only:
                    for poi in self.pois_to_fit:
                        poi_command = Command(
                            executable="combineTool.py",
                            input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                                self.name
                            ),
                            args=[
                                "--name _SCAN_1D{}".format(poi),
                                "-M MultiDimFit",
                                "-m 125.38",
                                "--algo grid",
                                acm_string,
                                # end ACM options
                                "--snapshotName MultiDimFit",
                                "--X-rtd MINIMIZER_freezeDisassociatedParams",
                                "--cminDefaultMinimizerStrategy 0",
                                "--floatOtherPOIs=1",
                                "--squareDistPoiStep",
                                "-P {}".format(poi),
                                "--setParameters {}".format(
                                    ",".join(
                                        ["{}=0".format(p) for p in self.main_model_pois]
                                    )
                                ),
                                "--redefineSignalPOIs {}".format(
                                    ",".join(self.pois_to_fit)
                                ),
                                "--points {}".format(points_one_dim),
                                "--split-points {}".format(split_points_one_dim)
                                if statonly
                                else "--split-points 2",
                                "--job-mode slurm",
                                '--sub-opts="--mem=15G"',
                                "--task-name _SCAN_{}_{}_{}_{}".format(
                                    model_name, submodel_name, poi, self.task_name
                                ),
                                "-v -1",
                                set_parameter_ranges_string,
                            ],
                        )
                        if self.pois_to_freeze:
                            poi_command.add_or_replace_option(
                                "--freezeParameters {}".format(
                                    ",".join(self.pois_to_freeze)
                                )
                            )
                        if statonly:
                            poi_command.add_or_replace_option(
                                "--freezeParameters allConstrainedNuisances",
                                force_add=True,
                            )
                        self.commands.append(poi_command)
                if not skip_2d:
                    for pois in combinations(self.pois_to_fit, 2):
                        poi_command = Command(
                            executable="combineTool.py",
                            input_file="higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                                self.name
                            ),
                            args=[
                                "--name _SCAN_2D{}".format("-".join(pois)),
                                "-M MultiDimFit",
                                "-m 125.38",
                                "--algo grid",
                                acm_string,
                                # end ACM options
                                "--snapshotName MultiDimFit",
                                "--X-rtd MINIMIZER_freezeDisassociatedParams",
                                "--cminDefaultMinimizerStrategy 0",
                                "--floatOtherPOIs=1",
                                "--squareDistPoiStep",
                                " ".join(["-P {}".format(p) for p in pois]),
                                "--setParameters {}".format(
                                    ",".join(
                                        ["{}=0".format(p) for p in self.main_model_pois]
                                    )
                                ),
                                "--redefineSignalPOIs {}".format(
                                    ",".join(self.pois_to_fit)
                                ),
                                "--points {}".format(points_two_dim),
                                "--split-points {}".format(split_points_two_dim),
                                "--job-mode slurm",
                                '--sub-opts="--mem=15G"',
                                "--task-name _SCAN_{}_{}_{}_{}".format(
                                    model_name,
                                    submodel_name,
                                    "-".join(pois),
                                    self.task_name,
                                ),
                                "-v -1",
                                set_parameter_ranges_string,
                            ],
                        )
                        if self.pois_to_freeze:
                            poi_command.add_or_replace_option(
                                "--freezeParameters {}".format(
                                    ",".join(self.pois_to_freeze)
                                )
                            )
                        if statonly:
                            poi_command.add_or_replace_option(
                                "--freezeParameters allConstrainedNuisances",
                                force_add=True,
                            )
                        self.commands.append(poi_command)


class Asimov(Observed):
    def __init__(
        self,
        model_name,
        submodel_name,
        input_dir,
        name,
        main_model_pois,
        pois_to_fit,
        submodel_pois=None,
        skip_2d=False,
        skip_scans=False,
        set_parameter_ranges_string="",
        twod_only=False,
        statonly=False,
        skip_best=False,
        points_one_dim=100,
        split_points_one_dim=5,
        points_two_dim=600,
        split_points_two_dim=4,
        tk=False,
    ):
        super(Asimov, self).__init__(
            model_name,
            submodel_name,
            input_dir,
            name,
            main_model_pois,
            pois_to_fit,
            submodel_pois=submodel_pois,
            skip_2d=skip_2d,
            skip_scans=skip_scans,
            set_parameter_ranges_string=set_parameter_ranges_string,
            twod_only=twod_only,
            statonly=statonly,
            skip_best=skip_best,
            points_one_dim=points_one_dim,
            split_points_one_dim=split_points_one_dim,
            points_two_dim=points_two_dim,
            split_points_two_dim=split_points_two_dim,
            tk=tk,
        )
        for command in self.commands:
            command.add_or_replace_option("--toys -1")

import os
import yaml
import logging

logger = logging.getLogger(__name__)

from .helpers import dict_to_Command_input


class Command:
    """ Construct a command in the form:
    executable 
        input_file
        -flag_arg1 arg1
        --flag_arg2 arg2
        --flag3
        --flag4
        ...

    """

    def __init__(self, executable, input_file, args):
        self.executable = executable
        self.input_file = input_file
        self.args = args
        self.full_command = " ".join([self.executable] + [self.input_file] + self.args)

    def run(self):
        os.system(self.full_command)

    def __repr__(self):
        r = "\n{}\n\t{}\n\t".format(self.executable, self.input_file) + "\n\t".join(
            self.args
        )
        return r

    def add_or_replace_option(self, arg, force_add=False):
        """ Add an option or change an existing one in case the first part of the flag is the same
        E.g. if we pass '--freezeParameters allConstrainedNuisances' and the command already has '--freezeParameters MH',
        the latter will be replaced by the former
        """
        first_part = arg.split()[0]
        for index, flag in enumerate(self.args):
            if flag.startswith(first_part):
                if force_add:
                    last_part = arg.split(" ")[-1]
                    arg = flag + ",{}".format(last_part)
                self.args[index] = arg
                logger.debug("Substituted {} by {}".format(flag, arg))
                self.full_command = " ".join(
                    [self.executable] + [self.input_file] + self.args
                )
                return

        logger.debug("Adding option {} to command \n{}".format(arg, self.full_command))
        self.args.append(arg)
        self.full_command = " ".join([self.executable] + [self.input_file] + self.args)


class Routine(object):
    """ Takes a list of Command objects and run them in order
    """

    def __init__(self, commands=None):
        if not commands:
            self.commands = []
        else:
            self.commands = commands

    def run(self):
        for command in self.commands:
            command.run()

    def __repr__(self):
        return "\n".join([repr(command) for command in self.commands])

    def add_option(self, arg):
        for command in self.commands:
            command.add_or_replace_option(arg)

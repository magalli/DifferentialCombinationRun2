import os
import yaml
from yaml import Loader, Dumper
from yaml.representer import SafeRepresenter
from collections import OrderedDict

_mapping_tag = yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG
import json
import logging

logger = logging.getLogger(__name__)

# see https://gist.github.com/oglops/c70fb69eef42d40bed06
def dict_representer(dumper, data):
    return dumper.represent_dict(data.iteritems())


def dict_constructor(loader, node):
    return OrderedDict(loader.construct_pairs(node))


Dumper.add_representer(OrderedDict, dict_representer)
Loader.add_constructor(_mapping_tag, dict_constructor)

Dumper.add_representer(str, SafeRepresenter.represent_str)

Dumper.add_representer(unicode, SafeRepresenter.represent_unicode)


def setup_logging(level=logging.INFO):
    logger = logging.getLogger()

    logger.setLevel(getattr(logging, level))
    formatter = logging.Formatter(
        "%(levelname)s - %(filename)s:%(lineno)s - %(funcName)s - %(message)s"
    )

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    return logger


def two_permutations(lst, lst_no_first=None, results=None):
    """ Quick way to return e.g.
    [(1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)]
    from [1, 2, 3, 4]
    """
    if lst_no_first is None:
        lst_no_first = lst[1:]
    if results is None:
        results = []
    if len(lst) == 1:
        return results
    first = lst[0]
    for second in lst_no_first:
        results.append((first, second))
    return two_permutations(lst[1:], lst_no_first[1:], results)


def pretty_ordered_dict(dct):
    """
    Pretty print an OrderedDict
    """
    return json.dumps(dct, indent=4, sort_keys=False)


def merge_two_dicts(x, y):
    # In Python3 we wouldn't need this shit... dioporcadiquellamadonna
    z = x.copy()
    z.update(y)
    return z


def get_nested_keys(dct):
    """
    Get all the sub-keys in a nested dictionary
    """
    nst_keys = []
    for key, value in dct.items():
        for k, v in dct.items():
            for kk in v:
                if kk not in nst_keys:
                    nst_keys.append(kk)
    return nst_keys


def create_and_access_nested_dir(path_to_dir):
    """ Create a nested directory for path_to_dir and access it.
    First, a check is performed and last "/" removed if present.
    Then, we create an absolute if path_to_dir is relative (i.e., if it does not start with "/").
    We then split the full path into base_dir and the last nested level (want_to_create).
    Ex:
    final_path = "/aa/bb/cc/dd"
    base_dir = "/aa/bb/cc"
    want_to_create = "dd"
    We then check if want_to_create is present; if it is, instead of "want_to_create", we create 
    want_to_create-$(N+1) where N is the highest number for which we have directories called
    want_to_create-$N inside base_dir
    """
    path_to_dir = path_to_dir[:-1] if path_to_dir.endswith("/") else path_to_dir
    if path_to_dir.startswith("/"):
        final_path = path_to_dir
    else:
        current_path = os.path.abspath(os.getcwd())
        final_path = "{}/{}".format(current_path, path_to_dir)

    base_dir, want_to_create = final_path.rsplit("/", 1)
    logger.debug("{}, {}".format(base_dir, want_to_create))
    mkdir(base_dir)
    already_in_base_dir = [s for s in os.listdir(base_dir) if want_to_create in s]
    if want_to_create in already_in_base_dir:
        logger.debug(
            "{} already present in {}".format(want_to_create, already_in_base_dir)
        )
        counter = 1
        while True:
            check = "{}-{}".format(want_to_create, counter)
            if check in already_in_base_dir:
                logger.debug(
                    "{} already present in {}".format(check, already_in_base_dir)
                )
                counter += 1
                continue
            else:
                want_to_create = check
                break

    final_path = "{}/{}".format(base_dir, want_to_create)
    logger.info("Creating {}".format(final_path))
    os.system("mkdir -p {}".format(final_path))
    logger.info("Moving to {}".format(final_path))
    os.chdir(final_path)

    return final_path


def extract_from_yaml_file(path_to_file):
    with open(path_to_file) as fl:
        # dct = yaml.load(fl, Loader=yaml.FullLoader)
        dct = yaml.load(fl, Loader=Loader)

    return dct


def write_to_yaml_file(path_to_file, obj):
    with open(path_to_file, "w") as fl:
        yaml.dump(obj, fl, default_flow_style=False)


def mkdir(dr):
    """make a directory (dr) if it doesn't exist"""
    if not os.path.exists(dr):
        os.makedirs(dr)


combine_metadata_model = {
    "path_to_root_file": None,
    "batch_config": {"job-mode": "slurm", "sub-opts": '"--mem=5G --time=20:00:00"'},
    "global_fit": {
        "options": {
            "method": "MultiDimFit",
            "mass": 125.0,
            "cminDefaultMinimizerType": "Minuit2",
            "cminDefaultMinimizerAlgo": "Migrad",
            "cminDefaultMinimizerStrategy": 0,
            "cminDefaultMinimizerTolerance": 0.1,
            "saveNLL": True,
            "saveInactivePOI": 1,
            "floatOtherPOIs": 1,
            "saveWorkspace": True,
        }
    },
    "fit_per_bin": {},
}

combine_metadata_fit_per_bin = {
    "default_value": 1.0,
    "range": [-1, 4],
    "options": {
        "method": "MultiDimFit",
        "mass": 125.0,
        "cminDefaultMinimizerType": "Minuit2",
        "cminDefaultMinimizerAlgo": "Migrad",
        "cminDefaultMinimizerStrategy": 0,
        "cminDefaultMinimizerTolerance": 0.1,
        "saveNLL": True,
        "saveInactivePOI": 1,
        "floatOtherPOIs": 1,
        "algo": "grid",
        "snapshotName": "MultiDimFit",
        "skipInitialFit": True,
        "points": 55,
        "split-points": 5,
    },
}


def get_channels_and_processes(datacard):
    """
    Extract the channels and processes from the datacard
    """
    with open(datacard, "r") as f:
        lines = f.readlines()

    for line in lines:
        if line.startswith("bin"):
            channels = line.split()[1:]
            break
    for line in lines:
        if line.startswith("process"):
            processes = line.split()[1:]
            break

    ws = {}
    for channel, process in zip(channels, processes):
        if channel not in ws:
            ws[channel] = []
        ws[channel].append(process)

    return ws

"""
Hgg has a much finer binning than the other decay modes. This means that, if we want to fit the maximum number
of signal strenghts, we will use the ones from Hgg itself. The implication is that reco bins of decay channels 
with coarser bins will have to be mapped to a combination of POIs, e.g.:
    map=*.hzz.*/ggH_PT_0_10=r_ggH_PT_0_5*ggH_PTH_0_5_SM/ggH_PTH_0_10_SM+r_ggH_5_10*ggH_PTH_5_10_SM/ggH_PTH_0_10_SM

The weights used for the linear combination of Hgg POIs are the ratio of the SM xs in that bin to the SM total xs.
These numbers are provided by Thomas inside TheoreticalPredictions/fullPSPred in form of pickle files where every list 
is itself a list where the three elements are the predictions for a Higgs mass of 120, 125 and 130 GeV respectively.
These values are already extrapolated to the full phase space, so to get a prediction for the SM production xs we 
have to divide it by the BR of Hgg.
"""

import numpy as np
import yaml
import scipy.interpolate as itr
import argparse
import pickle as pkl
from collections import OrderedDict as od

from differential_combination.utils import setup_logging
from differential_combination.utils import write_to_yaml_file


def parse_arguments():
    parser = argparse.ArgumentParser(description="")

    parser.add_argument(
        "--input-dir",
        type=str,
        default="DifferentialCombinationRun2/TheoreticalPredictions/fullPSPred",
        help="Input file with the predictions.",
    )

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    return parser.parse_args()


def create_align_bins_mapping_syntax(funct_name, lst):
    """
    lst = [("poi_1", sm_xs_1), ("poi_2", sm_xs_2), ...]

    return e.g.:
    merge_0_5_10_15=expr::merge_0_5_10_15("@0*0.3124356073520496+@1*0.6070402283561497+@2*0.61884345700122", r_smH_PTH_0_5, r_smH_PTH_5_10, r_smH_PTH_10_15)
    """
    if len(lst) == 1:
        return lst[0][0]

    container = 'merge_{}=expr::merge_{}("{}", {})'
    first_arg = "+".join(["@{}*{}".format(n, t[1]) for n, t in enumerate(lst)])
    second_arg = ",".join([item[0] for item in lst])

    return container.format(funct_name, funct_name, first_arg, second_arg)


def get_prediction(arr, mass, weights=None, interPRepl=None, massRepl=None):
    # Defined by Thomas in https://github.com/threiten/HiggsAnalysis-CombinedLimit/blob/d5d9ef377a7c69a8d4eaa366b47e7c81931e71d9/test/plotBinnedSigStr.py#L236
    # To be used with weights [1, 2.3, 1]
    nBins = len(arr)
    masses = [120.0, 125.0, 130.0]
    if weights is None:
        weights = np.ones(arr.shape[1])
    splines = []
    if arr.shape[1] == 1:
        if interPRepl is None or massRepl is None:
            raise ValueError(
                "If only one masspoint is given, interPRepl and massRepl must be provided!"
            )
        for i in range(nBins):
            splines.append(
                itr.UnivariateSpline(masses, interPRepl[i, :], w=weights, k=2)
            )

        return np.array(
            [
                splines[i](mass) - interPRepl[i, masses.index(massRepl)] + arr[i, 0]
                for i in range(nBins)
            ]
        )

    for i in range(nBins):
        splines.append(itr.UnivariateSpline(masses, arr[i, :], w=weights, k=2))

    return np.array([splines[i](mass) for i in range(nBins)])


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")

    hgg_br = 0.0023
    mass = 125.38
    weights = [1.0, 2.3, 1.0]

    # observable: smH_PTH
    # category: Hgg_HZZ
    mapping_dict = od()

    observable = "smH_PTH"
    category = "Hgg_HZZ"

    hgg = od()

    hgg[".*hgg.*/smH_PTH_0p0_5p0"] = "r_smH_PTH_0_5"
    hgg[".*hgg.*/smH_PTH_5p0_10p0"] = "r_smH_PTH_5_10"
    hgg[".*hgg.*/smH_PTH_10p0_15p0"] = "r_smH_PTH_10_15"
    hgg[".*hgg.*/smH_PTH_15p0_20p0"] = "r_smH_PTH_15_20"
    hgg[".*hgg.*/smH_PTH_20p0_25p0"] = "r_smH_PTH_20_25"
    hgg[".*hgg.*/smH_PTH_25p0_30p0"] = "r_smH_PTH_25_30"
    hgg[".*hgg.*/smH_PTH_30p0_35p0"] = "r_smH_PTH_30_35"
    hgg[".*hgg.*/smH_PTH_35p0_45p0"] = "r_smH_PTH_35_45"
    hgg[".*hgg.*/smH_PTH_45p0_60p0"] = "r_smH_PTH_45_60"
    hgg[".*hgg.*/smH_PTH_60p0_80p0"] = "r_smH_PTH_60_80"
    hgg[".*hgg.*/smH_PTH_80p0_100p0"] = "r_smH_PTH_80_100"
    hgg[".*hgg.*/smH_PTH_100p0_120p0"] = "r_smH_PTH_100_120"
    hgg[".*hgg.*/smH_PTH_120p0_140p0"] = "r_smH_PTH_120_140"
    hgg[".*hgg.*/smH_PTH_140p0_170p0"] = "r_smH_PTH_140_170"
    hgg[".*hgg.*/smH_PTH_170p0_200p0"] = "r_smH_PTH_170_200"
    hgg[".*hgg.*/smH_PTH_200p0_250p0"] = "r_smH_PTH_200_250"
    hgg[".*hgg.*/smH_PTH_250p0_350p0"] = "r_smH_PTH_250_350"
    hgg[".*hgg.*/smH_PTH_350p0_450p0"] = "r_smH_PTH_350_450"
    hgg[".*hgg.*/smH_PTH_450p0_1000p0"] = "r_smH_PTH_GT450"

    for key, value in hgg.items():
        mapping_dict[key] = value + "[1.0,-1.0,4.0]"

    with open(args.input_dir + "/theoryPred_Pt_18_fullPS.pkl", "r") as f:
        arr = pkl.load(f)

    fps_xs = get_prediction(arr, mass, weights=weights)
    logger.info("fps_xs_norm_br: {}".format(fps_xs))

    hgg_predictions = od()
    for poi, xs in zip(hgg.values(), fps_xs):
        hgg_predictions[poi] = xs

    hzz_proc_2_hgg_xs = od()
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_0p0_15p0"] = [
        "r_smH_PTH_0_5",
        "r_smH_PTH_5_10",
        "r_smH_PTH_10_15",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_15p0_30p0"] = [
        "r_smH_PTH_15_20",
        "r_smH_PTH_20_25",
        "r_smH_PTH_25_30",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_30p0_45p0"] = [
        "r_smH_PTH_30_35",
        "r_smH_PTH_35_45",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_45p0_80p0"] = [
        "r_smH_PTH_45_60",
        "r_smH_PTH_60_80",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_80p0_120p0"] = [
        "r_smH_PTH_80_100",
        "r_smH_PTH_100_120",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_120p0_200p0"] = [
        "r_smH_PTH_120_140",
        "r_smH_PTH_140_170",
        "r_smH_PTH_170_200",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_200p0_350p0"] = [
        "r_smH_PTH_200_250",
        "r_smH_PTH_250_350",
    ]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_350p0_450p0"] = ["r_smH_PTH_350_450"]
    hzz_proc_2_hgg_xs[".*hzz.*/smH_PTH_GT450"] = ["r_smH_PTH_GT450"]

    n_contributors = [len(hzz_proc_2_hgg_xs[key]) for key in hzz_proc_2_hgg_xs.keys()]
    logger.info("n_contributors: {}".format(n_contributors))

    counter = 0
    hgg_values_correct = []
    hgg_values = np.array(list(hgg_predictions.values()))
    for n in n_contributors:
        xs_range = hgg_values[counter : counter + n]
        hgg_values_correct.extend(xs_range / np.sum(xs_range))
        counter += n

    hgg_predictions_correct = od()
    for poi, xs in zip(hgg.values(), hgg_values_correct):
        hgg_predictions_correct[poi] = xs

    for n, key_value in enumerate(hzz_proc_2_hgg_xs.items()):
        key, value = key_value
        lst = [(k, v) for k, v in hgg_predictions_correct.items() if k in value]
        mapping_dict[key] = create_align_bins_mapping_syntax(n, lst)

    hww_proc_2_hgg_xs = od()
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_0_15"] = [
        "r_smH_PTH_0_5",
        "r_smH_PTH_5_10",
        "r_smH_PTH_10_15",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_15_30"] = [
        "r_smH_PTH_15_20",
        "r_smH_PTH_20_25",
        "r_smH_PTH_25_30",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_30_45"] = [
        "r_smH_PTH_30_35",
        "r_smH_PTH_35_45",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_45_80"] = [
        "r_smH_PTH_45_60",
        "r_smH_PTH_60_80",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_80_120"] = [
        "r_smH_PTH_80_100",
        "r_smH_PTH_100_120",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_120_200"] = [
        "r_smH_PTH_120_140",
        "r_smH_PTH_140_170",
        "r_smH_PTH_170_200",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_200_350"] = [
        "r_smH_PTH_200_250",
        "r_smH_PTH_250_350",
    ]
    hww_proc_2_hgg_xs[".*hww.*/.*H_hww_PTH_GT350"] = [
        "r_smH_PTH_350_450",
        "r_smH_PTH_GT450",
    ]

    n_contributors = [len(hww_proc_2_hgg_xs[key]) for key in hww_proc_2_hgg_xs.keys()]
    logger.info("n_contributors: {}".format(n_contributors))

    counter = 0
    hgg_values_correct = []
    hgg_values = np.array(list(hgg_predictions.values()))
    for n in n_contributors:
        xs_range = hgg_values[counter : counter + n]
        hgg_values_correct.extend(xs_range / np.sum(xs_range))
        counter += n

    hgg_predictions_correct = od()
    for poi, xs in zip(hgg.values(), hgg_values_correct):
        hgg_predictions_correct[poi] = xs

    for n, key_value in enumerate(hww_proc_2_hgg_xs.items()):
        key, value = key_value
        lst = [(k, v) for k, v in hgg_predictions_correct.items() if k in value]
        mapping_dict[key] = create_align_bins_mapping_syntax(n, lst)

    htt_proc_2_hgg_xs = od()
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_0_45.*"] = [
        "r_smH_PTH_0_5",
        "r_smH_PTH_5_10",
        "r_smH_PTH_10_15",
        "r_smH_PTH_15_20",
        "r_smH_PTH_20_25",
        "r_smH_PTH_25_30",
        "r_smH_PTH_30_35",
        "r_smH_PTH_35_45",
    ]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_45_80.*"] = [
        "r_smH_PTH_45_60",
        "r_smH_PTH_60_80",
    ]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_80_120.*"] = [
        "r_smH_PTH_80_100",
        "r_smH_PTH_100_120",
    ]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_120_140.*"] = ["r_smH_PTH_120_140"]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_140_170.*"] = ["r_smH_PTH_140_170"]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_170_200.*"] = ["r_smH_PTH_170_200"]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_200_350.*"] = [
        "r_smH_PTH_200_250",
        "r_smH_PTH_250_350",
    ]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_350_450.*"] = ["r_smH_PTH_350_450"]
    htt_proc_2_hgg_xs[".*htt.*/.*H.*PTH_G.450.*"] = ["r_smH_PTH_GT450"]

    n_contributors = [len(htt_proc_2_hgg_xs[key]) for key in htt_proc_2_hgg_xs.keys()]
    logger.info("n_contributors: {}".format(n_contributors))

    counter = 0
    hgg_values_correct = []
    hgg_values = np.array(list(hgg_predictions.values()))
    for n in n_contributors:
        xs_range = hgg_values[counter : counter + n]
        hgg_values_correct.extend(xs_range / np.sum(xs_range))
        counter += n

    hgg_predictions_correct = od()
    for poi, xs in zip(hgg.values(), hgg_values_correct):
        hgg_predictions_correct[poi] = xs

    for n, key_value in enumerate(htt_proc_2_hgg_xs.items()):
        key, value = key_value
        lst = [(k, v) for k, v in hgg_predictions_correct.items() if k in value]
        mapping_dict[key] = create_align_bins_mapping_syntax(n, lst)

    logger.info(
        "mapping_dict:\n{}".format(
            "\n".join("{}: {}".format(k, v) for k, v in mapping_dict.items())
        )
    )


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

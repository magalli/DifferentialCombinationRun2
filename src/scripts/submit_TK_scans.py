import argparse
import os
from datetime import datetime

from differential_combination.command.SMEFT.base_commands import (
    AsimovWithEnvelope,
    Observed,
    Asimov,
)
from differential_combination.utils import (
    setup_logging,
    extract_from_yaml_file,
    create_and_access_nested_dir,
)


Yukawa_NOTscalingbbH_floatingBRs = "Yukawa_NOTscalingbbH_floatingBRs"
Yukawa_NOTscalingbbH_couplingdependentBRs = "Yukawa_NOTscalingbbH_couplingdependentBRs"
Top_scalingttH_floatingBRs = "Top_scalingttH_floatingBRs"
Top_scalingttH_couplingdependentBRs = "Top_scalingttH_couplingdependentBRs"
Top_scalingbbHttH_floatingBRs = "Top_scalingbbHttH_floatingBRs"
Top_scalingbbHttH_couplingdependentBRs = "Top_scalingbbHttH_couplingdependentBRs"


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Submit scans for SMEFT interpretations"
    )

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--model",
        required=True,
        type=str,
        choices=[
            Yukawa_NOTscalingbbH_floatingBRs,
            Yukawa_NOTscalingbbH_couplingdependentBRs,
            Top_scalingttH_floatingBRs,
            Top_scalingttH_couplingdependentBRs,
            Top_scalingbbHttH_floatingBRs,
            Top_scalingbbHttH_couplingdependentBRs,
        ],
        help="Model used for the fit",
    )

    parser.add_argument(
        "--category", required=True, type=str, help="Category for which submit scans"
    )

    parser.add_argument(
        "--input-dir",
        type=str,
        help="Directory where the .root file containing the workspaces is stored",
    )

    parser.add_argument(
        "--output-dir",
        type=str,
        default="outputs",
        help="Directory where output files will be stored",
    )

    parser.add_argument(
        "--skip-2d",
        action="store_true",
        required=False,
        default=False,
        help="Skip 2D scans",
    )

    parser.add_argument(
        "--skip-scans",
        action="store_true",
        required=False,
        default=False,
        help="Skip scans",
    )

    parser.add_argument(
        "--twod-only",
        action="store_true",
        required=False,
        default=False,
        help="Only run 2D scans",
    )

    parser.add_argument(
        "--global-fit-dir",
        type=str,
        default=None,
        help="Directory where the global fit is stored",
    )

    parser.add_argument(
        "--custom-range",
        type=str,
        required=False,
        default=None,
        help="Custom range for the scan",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    logger.info("Starting scans submission for SMEFT interpretation")

    initial_path = os.path.abspath(os.getcwd())
    if args.input_dir.startswith("/"):
        input_dir = args.input_dir
    else:
        input_dir = "{}/{}".format(initial_path, args.input_dir)

    # Pick routine and create
    which_routine = Observed
    if "Hgg" in args.category and "asimov" in args.category:
        which_routine = AsimovWithEnvelope
    elif "asimov" in args.category and "Hgg" not in args.category:
        which_routine = Asimov

    base_category = args.category.split("_")[0]

    pois = {
        Yukawa_NOTscalingbbH_floatingBRs: ["kappac", "kappab"],
        Yukawa_NOTscalingbbH_couplingdependentBRs: ["kappac", "kappab"],
        Top_scalingttH_floatingBRs: ["ct", "cg"],
        Top_scalingttH_couplingdependentBRs: ["ct", "cg"],
        Top_scalingbbHttH_floatingBRs: ["ct", "cb"],
        Top_scalingbbHttH_couplingdependentBRs: ["ct", "cb"],
    }
    default_ranges_strings = {
        Yukawa_NOTscalingbbH_floatingBRs: {
            "HZZ_asimov": "--setParameterRanges kappab=-22.0,22.0:kappac=-35.0,35.0",
            "HggHZZHWWHtt_asimov": "--setParameterRanges kappab=-5.0,15.0:kappac=-30.0,30.0",
        },
        # Yukawa_NOTscalingbbH_couplingdependentBRs: "--setParameterRanges kappab=-2.0,2.0:kappac=-8.0,8.0",
        # Top_scalingttH_floatingBRs: "--setParameterRanges ct=-3.0,3.0:cg=-0.2,0.2",
        # Top_scalingttH_couplingdependentBRs: "--setParameterRanges ct=-0.4,4.4:cg=-0.28,0.1",
        Top_scalingbbHttH_floatingBRs: {
            "Hgg_asimov": "--setParameterRanges ct=-4.0,4.0:cb=-15.0,15.0",
            "HZZ_asimov": "--setParameterRanges ct=-4.0,4.0:cb=-15.0,15.0",
            "HggHZZHWWHtt_asimov": "--setParameterRanges ct=-4.0,4.0:cb=-15.0,15.0",
        },
        Top_scalingbbHttH_couplingdependentBRs: {
            "Hgg_asimov": "--setParameterRanges ct=-0.0,4.5:cb=-2.0,2.0",
            "HZZ_asimov": "--setParameterRanges ct=-4.5,4.5:cb=-3.0,3.0",
            "HggHZZHWWHtt_asimov": "--setParameterRanges ct=-0.3,3.0:cb=-3.0,3.0",
            "HggHZZHWWHttHttBoost_asimov": "--setParameterRanges ct=-0.3,3.0:cb=-3.0,3.0",
        },
    }
    job_specs = {
        Yukawa_NOTscalingbbH_floatingBRs: {
            "HZZ_asimov": {
                "points_one_dim": 160,
                "split_points_one_dim": 2,
                "points_two_dim": 800,
                "split_points_two_dim": 10,
            },
            "HggHZZHWWHtt_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 2,
                "points_two_dim": 300,
                "split_points_two_dim": 2,
            },
        },
        Top_scalingttH_floatingBRs: {
            "HggHZZHWWHtt_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 1,
                "points_two_dim": 600,
                "split_points_two_dim": 1,
            }
        },
        Top_scalingbbHttH_floatingBRs: {
            "Hgg_asimov": {
                "points_one_dim": 200,
                "split_points_one_dim": 2,
                "points_two_dim": 800,
                "split_points_two_dim": 2,
            },
            "HZZ_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 10,
                "points_two_dim": 800,
                "split_points_two_dim": 10,
            },
            "HggHZZHWWHtt_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 1,
                "points_two_dim": 800,
                "split_points_two_dim": 2,
            },
        },
        Top_scalingbbHttH_couplingdependentBRs: {
            "Hgg_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 1,
                "points_two_dim": 300,
                "split_points_two_dim": 1,
            },
            "HZZ_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 5,
                "points_two_dim": 400,
                "split_points_two_dim": 5,
            },
            "HggHZZHWWHtt_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 1,
                "points_two_dim": 800,
                "split_points_two_dim": 2,
            },
            "HggHZZHWWHttHttBoost_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 1,
                "points_two_dim": 800,
                "split_points_two_dim": 2,
            },
        },
    }
    extra_options = {
        Yukawa_NOTscalingbbH_floatingBRs: {
            "HZZ_asimov": ["--setParameters kappac=1.0,kappab=1.0"],
            "HggHZZHWWHtt_asimov": ["--setParameters kappac=1.0,kappab=1.0"],
        },
        Top_scalingttH_floatingBRs: {
            "HggHZZHWWHttHbb_asimov": ["--setParameters ct=1.0,cg=0.0"]
        },
        Top_scalingbbHttH_floatingBRs: {
            "Hgg_asimov": ["--setParameters ct=1.0,cb=1.0"],
            "HZZ_asimov": ["--setParameters ct=1.0,cb=1.0"],
            "HggHZZHWWHtt_asimov": ["--setParameters ct=1.0,cb=1.0"],
        },
        Top_scalingbbHttH_couplingdependentBRs: {
            "Hgg_asimov": ["--setParameters ct=1.0,cb=1.0"],
            "HZZ_asimov": ["--setParameters ct=2.0,cb=1.0"],
            "HggHZZHWWHtt_asimov": ["--setParameters ct=1.0,cb=1.0"],
            "HggHZZHWWHttHttBoost_asimov": ["--setParameters ct=1.0,cb=1.0"],
        },
    }

    routine = which_routine(
        model_name=args.model,
        submodel_name="",
        input_dir=input_dir,
        name="{}_{}".format(args.model, base_category),
        main_model_pois=pois[args.model],
        pois_to_fit=pois[args.model],
        submodel_pois=pois[args.model],
        skip_2d=args.skip_2d,
        skip_scans=args.skip_scans,
        set_parameter_ranges_string=default_ranges_strings[args.model][args.category]
        if args.custom_range is None
        else args.custom_range,
        twod_only=args.twod_only,
        statonly=True if "statonly" in args.category else False,
        skip_best=True if args.global_fit_dir else False,
        points_one_dim=job_specs[args.model][args.category]["points_one_dim"]
        if args.model in job_specs and args.category in job_specs[args.model]
        else 100,
        split_points_one_dim=job_specs[args.model][args.category][
            "split_points_one_dim"
        ]
        if args.model in job_specs and args.category in job_specs[args.model]
        else 5,
        points_two_dim=job_specs[args.model][args.category]["points_two_dim"]
        if args.model in job_specs and args.category in job_specs[args.model]
        else 600,
        split_points_two_dim=job_specs[args.model][args.category][
            "split_points_two_dim"
        ]
        if args.model in job_specs and args.category in job_specs[args.model]
        else 4,
        tk=True,
    )

    # add extra options
    exop = extra_options[args.model][args.category]

    for ex in exop:
        for command in routine.commands:
            command.add_or_replace_option(ex)

    logger.info("Built the following XSRoutine: \n{}".format(routine))
    logger.debug(
        "One line versions in case you want to copy:\n\n{}".format(
            "\n\n".join([c.full_command for c in routine.commands])
        )
    )

    # Create output subdirs and move to output dir
    output_path = "{}/{}/{}-{}".format(
        args.output_dir,
        args.model,
        args.category,
        datetime.today().strftime("%Y%m%dxxx%H%M%S"),
    )

    new_output_path = create_and_access_nested_dir(output_path)
    if args.global_fit_dir:
        logger.info(
            "Copying global fit files from {} to {}".format(
                args.global_fit_dir, new_output_path
            )
        )
        if "Hgg" in args.category and "asimov" in args.category:
            for fl in [
                "higgsCombineAsimovBestFit.MultiDimFit.mH125.38.root",
                "higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                "higgsCombineAsimovPreFit.GenerateOnly.mH125.38.123456.root",
            ]:
                os.system(
                    "cp {}/{} {}".format(args.global_fit_dir, fl, new_output_path)
                )
        else:
            base_category = args.category.split("_")[0]
            global_fit_file = "higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                base_category
            )
            os.system(
                "cp {}/{} {}".format(
                    args.global_fit_dir, global_fit_file, new_output_path
                )
            )

    if args.debug:
        logger.debug("Removing {} as it won't be used".format(new_output_path))
        os.rmdir(new_output_path)

    # Run routine (submit jobs)
    if not args.debug:
        routine.run()


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

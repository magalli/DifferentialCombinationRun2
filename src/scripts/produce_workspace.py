""" Run commands in the format:
[   'text2workspace.py',
    'Analyses/hig-19-002/ptH/fullmodel.txt',
    '-o CombinedWorkspaces/smH_PTH_HWW_ws.root',
    '-P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel',
    "--PO 'map=.*/smH_PTH_0_20:r_smH_PTH_0_20[1.0,-1.0,4.0]'",
    "--PO 'map=.*/smH_PTH_20_45:r_smH_PTH_20_45[1.0,-1.0,4.0]'",
    "--PO 'map=.*/smH_PTH_45_80:r_smH_PTH_45_80[1.0,-1.0,4.0]'",
    "--PO 'map=.*/smH_PTH_120_200:r_smH_PTH_120_200[1.0,-1.0,4.0]'",
    "--PO 'map=.*/smH_PTH_80_120:r_smH_PTH_80_120[1.0,-1.0,4.0]'",
    "--PO 'map=.*/smH_PTH_GT200:r_smH_PTH_GT200[1.0,-1.0,4.0]'"]

The main part consists in the mapping between the gen bins and the reco bins;
if the required argument --mapping exists, it has to be in the following format

ggH_PTH_0_45: r_smH_PTH_0_45
xH_PTH_0_45: r_smH_PTH_0_45
ggH_PTH_45_80: r_smH_PTH_45_80
xH_PTH_45_80: r_smH_PTH_45_80
ggH_PTH_80_120: r_smH_PTH_80_120
xH_PTH_80_120: r_smH_PTH_80_120
ggH_PTH_120_200: r_smH_PTH_120_200
xH_PTH_120_200: r_smH_PTH_120_200
ggH_PTH_200_350: r_smH_PTH_200_350
xH_PTH_200_350: r_smH_PTH_200_35

and it is used to set the POIs; if it does not exist, they are inferred and the path is used to generate 
a mapping YAML file.

An example of command looks like the following:

produce_workspace.py --datacard Analyses/hig-19-002/ptH/fullmodel.txt --observable smH_PTH --category HWW

Since the argument --output-dir is not passed and it defaults to CombinedWorkspaces, the ROOT file created will be:
CombinedWorkspaces/smH_PTH_HWW_ws.root

In the second part, if the argument --combine-metadata-dir is provided, a yaml file containing the arguments for the scan submission
is dumped.
"""
import argparse
import pprint
import os
import re
import yaml
from copy import deepcopy

from differential_combination.meta.mappings import mappings
from differential_combination.utils import (
    setup_logging,
    mkdir,
    pretty_ordered_dict,
    get_nested_keys,
)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Produce workspaces")

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--datacard", required=True, type=str, help="Datacard converted to workspace"
    )

    parser.add_argument(
        "--model",
        required=True,
        type=str,
        choices=list(mappings.keys()),
        help="Model used for the fit",
    )

    parser.add_argument(
        "--observable",
        required=True,
        type=str,
        choices=list(mappings["SM"].keys()),
        help="Observable considered",
    )

    parser.add_argument(
        "--category",
        required=True,
        type=str,
        choices=get_nested_keys(mappings["SM"]),
        help="Category",
    )

    parser.add_argument(
        "--validate",
        action="store_true",
        default=False,
        help="Print the command without running it",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    pp = pprint.PrettyPrinter(indent=4)

    output_dir = os.path.join(
        "DifferentialCombinationRun2/CombinedWorkspaces", args.model, args.observable
    )
    mkdir(output_dir)
    output_file = "{}.root".format(args.category)

    command = [
        "text2workspace.py",
        args.datacard,
        "-o {}/{}".format(output_dir, output_file),
        "-m 125" if args.category in ["Htt", "HttBoost"] else "-m 125.38",
        "-P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel",
    ]

    mapping_list = mappings[args.model][args.observable][args.category]

    logger.info(
        "Passed/generated mapping: \n{}".format(pretty_ordered_dict(mapping_list))
    )

    # Append to command
    if args.category not in ["HWW", "Htt", "HbbVBF"]:
        command.append(
            "--PO 'higgsMassRange=123,127'"
        )  # Thomas does it, so I do it too
    command += mapping_list

    logger.info("Command: \n{}".format(pp.pformat(command)))

    if not args.validate:
        # have to use this as subprocess doesn't handle special characters correctly
        os.system(" ".join(command))


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

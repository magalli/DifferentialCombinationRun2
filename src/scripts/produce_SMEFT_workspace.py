import argparse
import pprint
import os
import yaml

from differential_combination.utils import (
    setup_logging,
    mkdir,
    extract_from_yaml_file,
    pretty_ordered_dict,
)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Produce workspaces")

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--datacard", required=True, type=str, help="Datacard converted to workspace"
    )

    parser.add_argument(
        "--config-file",
        required=True,
        type=str,
        help="Path to a yaml file containing the model",
    )

    parser.add_argument(
        "--equations-dir",
        required=True,
        type=str,
        help="Path to a directory in Matt's repo containing the equations",
    )

    parser.add_argument(
        "--observable", required=True, type=str, help="Observable considered"
    )

    parser.add_argument("--category", required=True, type=str, help="Category")

    parser.add_argument(
        "--validate",
        action="store_true",
        default=False,
        help="Print the command without running it",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    pp = pprint.PrettyPrinter(indent=4)

    model_name = args.config_file.split("/")[-1].split(".")[0]
    logger.info("Model name: {}".format(model_name))

    output_dir = os.path.join(
        "DifferentialCombinationRun2/CombinedWorkspaces/SMEFT",
        args.observable,
        model_name,
    )
    mkdir(output_dir)
    output_file = "{}.root".format(args.category)

    command = [
        "text2workspace.py",
        args.datacard,
        "-o {}/{}".format(output_dir, output_file),
        "-P HiggsAnalysis.CombinedLimit.DIFFtoSMEFTModel:diff_to_smeft_model",
        "--PO input_dir={}".format(args.equations_dir),
        "--PO config_file={}".format(args.config_file),
    ]

    logger.info("Command: \n{}".format(pp.pformat(command)))

    if args.validate:
        logger.info(" ".join(command))
    else:
        os.system(" ".join(command))


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

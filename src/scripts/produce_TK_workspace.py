import argparse
from lib2to3.pytree import Base

from differential_combination.utils import setup_logging
from differential_combination.command.command import Command
import os
import numpy as np
import scipy.interpolate as itr
import pickle as pkl

Yukawa_NOTscalingbbH_floatingBRs = "Yukawa_NOTscalingbbH_floatingBRs"
Yukawa_NOTscalingbbH_couplingdependentBRs = "Yukawa_NOTscalingbbH_couplingdependentBRs"
Top_scalingttH_floatingBRs = "Top_scalingttH_floatingBRs"
Top_scalingttH_couplingdependentBRs = "Top_scalingttH_couplingdependentBRs"
Top_scalingbbHttH_floatingBRs = "Top_scalingbbHttH_floatingBRs"
Top_scalingbbHttH_couplingdependentBRs = "Top_scalingbbHttH_couplingdependentBRs"


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Produce workspaces for TK interpretation"
    )

    parser.add_argument(
        "--model",
        required=True,
        type=str,
        choices=[
            Yukawa_NOTscalingbbH_floatingBRs,
            Yukawa_NOTscalingbbH_couplingdependentBRs,
            Top_scalingttH_floatingBRs,
            Top_scalingttH_couplingdependentBRs,
            Top_scalingbbHttH_floatingBRs,
            Top_scalingbbHttH_couplingdependentBRs,
        ],
        help="Model used for the fit",
    )

    parser.add_argument(
        "--category",
        required=True,
        type=str,
        choices=[
            "HggHZZHWWHttHttBoost",
            "HggHZZHWWHttHbb",
            "HggHZZHWWHtt",
            "Hgg",
            "HZZ",
            "HbbVBF",
            "HttBoost",
        ],
        help="Category",
    )

    parser.add_argument(
        "--validate",
        action="store_true",
        default=False,
        help="Print the command without running it",
    )

    return parser.parse_args()


def main(args):
    logger = setup_logging(level="INFO")

    executable = "text2workspace.py"
    base_args = [
        "-P differential_combination.tk.physicsModels.CouplingModel:couplingModel",
        "--PO 'higgsMassRange=123,127'",
    ]
    base_args_scenario = {
        Yukawa_NOTscalingbbH_floatingBRs: [
            "--PO linearTerms=True",
            "--PO splitggH=True",
            "--PO SM=[kappab=1.0,kappac=1.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_1_kappac_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=-1.0,kappac=-5.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_m1_kappac_m5_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=2.0,kappac=1.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_2_kappac_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=2.0,kappac=0.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_2_kappac_0_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=0.0,kappac=-5.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_0_kappac_m5_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=0.0,kappac=5.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_0_kappac_5_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=-1.0,kappac=-10.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_m1_kappac_m10_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=0.0,kappac=-10.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_0_kappac_m10_muR_1_muF_1_Q_1.txt]",
            "--PO freely_floating_BRs=True",
            "--PO model_type=Yukawa",
        ],
        Yukawa_NOTscalingbbH_couplingdependentBRs: [
            "--PO linearTerms=True",
            "--PO splitggH=True",
            "--PO SM=[kappab=1.0,kappac=1.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_1_kappac_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=-1.0,kappac=-5.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_m1_kappac_m5_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=2.0,kappac=1.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_2_kappac_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=2.0,kappac=0.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_2_kappac_0_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=0.0,kappac=-5.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_0_kappac_m5_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=0.0,kappac=5.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_0_kappac_5_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=-1.0,kappac=-10.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_m1_kappac_m10_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[kappab=0.0,kappac=-10.0,file=DifferentialCombinationRun2/TKPredictions/theories_yukawa_summed/yukawa_summed_kappab_0_kappac_m10_muR_1_muF_1_Q_1.txt]",
            "--PO BRs_kappa_dependent=True",
            "--PO model_type=Yukawa",
        ],
        Top_scalingttH_floatingBRs: [
            "--PO linearTerms=False",
            "--PO splitggH=True",
            "--PO SM=[ct=1,cg=0,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1_cg_0_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.1,cg=0.075,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p1_cg_0p075_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=2.0,cg=-0.083,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_2_cg_m0p083_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.5,cg=0.042,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p5_cg_0p042_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=1.5,cg=-0.042,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1p5_cg_m0p042_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO add_scaling_ttH=True",
            "--PO freely_floating_BRs=True",
            "--PO model_type=Top",
            "--PO constrain_ratio_bb_ZZ=True",
        ],
        Top_scalingttH_couplingdependentBRs: [
            "--PO linearTerms=False",
            "--PO splitggH=True",
            "--PO SM=[ct=1,cg=0,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1_cg_0_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.1,cg=0.075,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p1_cg_0p075_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=2.0,cg=-0.083,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_2_cg_m0p083_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.5,cg=0.042,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p5_cg_0p042_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=1.5,cg=-0.042,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1p5_cg_m0p042_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO add_scaling_ttH=True",
            "--PO BRs_kappa_dependent=True",
            "--PO skipOverflowBin=False",
            "--PO model_type=Top",
        ],
        Top_scalingbbHttH_floatingBRs: [
            "--PO linearTerms=False",
            "--PO splitggH=True",
            "--PO SM=[ct=1,cb=1,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1_cg_0_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.8,cb=-3.67,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p8_cg_0_cb_m3p67_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=1.1,cb=3.79,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1p1_cg_0_cb_3p79_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=1.2,cb=4.67,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1p2_cg_0_cb_4p67_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.5,cb=-7.46,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p5_cg_0_cb_m7p46_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.9,cb=-1.79,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p9_cg_0_cb_m1p79_muR_1_muF_1_Q_1.txt]",
            "--PO add_scaling_ttH=True",
            "--PO add_scaling_bbH=True",
            "--PO freely_floating_BRs=True",
            "--PO skipOverflowBin=False",
            "--PO model_type=Top",
        ],
        Top_scalingbbHttH_couplingdependentBRs: [
            "--PO linearTerms=False",
            "--PO splitggH=True",
            "--PO SM=[ct=1,cb=1,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1_cg_0_cb_1_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.8,cb=-3.67,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p8_cg_0_cb_m3p67_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=1.1,cb=3.79,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1p1_cg_0_cb_3p79_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=1.2,cb=4.67,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_1p2_cg_0_cb_4p67_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.5,cb=-7.46,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p5_cg_0_cb_m7p46_muR_1_muF_1_Q_1.txt]",
            "--PO theory=[ct=0.9,cb=-1.79,file=DifferentialCombinationRun2/TKPredictions/theories_tophighpt/tophighpt_ct_0p9_cg_0_cb_m1p79_muR_1_muF_1_Q_1.txt]",
            "--PO add_scaling_ttH=True",
            "--PO add_scaling_bbH=True",
            "--PO BRs_kappa_dependent=True",
            "--PO skipOverflowBin=False",
            "--PO model_type=Top",
        ],
    }
    metadata_base_dir = "DifferentialCombinationRun2/metadata/TK"
    for scenario in ["Yukawa", "Top"]:
        for full_model_name in base_args_scenario.keys():
            if scenario in full_model_name:
                base_args_scenario[full_model_name].append(
                    "--PO binBoundaries={}/bin_boundaries/{}.yml".format(
                        metadata_base_dir, scenario
                    )
                )
                base_args_scenario[full_model_name].append(
                    "--PO SMXS_of_input_ws={}/SMXS/{}.yml".format(
                        metadata_base_dir, scenario
                    )
                )
                base_args_scenario[full_model_name].append(
                    "--PO correlationMatrix={}/correlation_matrices_paths/{}.yml".format(
                        metadata_base_dir, scenario
                    )
                )
                base_args_scenario[full_model_name].append(
                    "--PO theoryUncertainties={}/theory_uncertainties_paths/{}.yml".format(
                        metadata_base_dir, scenario
                    )
                )

    commands = {
        Yukawa_NOTscalingbbH_floatingBRs: {
            "HggHZZHWWHtt": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Yukawa_NOTscalingbbH_HggHZZHWWHtt.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Yukawa_NOTscalingbbH_floatingBRs_HggHZZHWWHtt.root"
                ]
                + base_args
                + base_args_scenario[Yukawa_NOTscalingbbH_floatingBRs],
            ),
            "Hgg": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Yukawa.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Yukawa_NOTscalingbbH_floatingBRs_Hgg.root"
                ]
                + base_args
                + base_args_scenario[Yukawa_NOTscalingbbH_floatingBRs]
                + ["--PO isOnlyHgg=True"],
            ),
            "HZZ": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/smH_PTH_HZZ_processRenumbered_TK.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Yukawa_NOTscalingbbH_floatingBRs_HZZ.root"
                ]
                + base_args
                + base_args_scenario[Yukawa_NOTscalingbbH_floatingBRs]
                + ["--PO isOnlyHZZ=True"],
            ),
        },
        Yukawa_NOTscalingbbH_couplingdependentBRs: {
            "HggHZZHWWHtt": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Yukawa_NOTscalingbbH_HggHZZHWWHtt.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Yukawa_NOTscalingbbH_couplingdependentBRs_HggHZZHWWHtt.root"
                ]
                + base_args
                + base_args_scenario[Yukawa_NOTscalingbbH_couplingdependentBRs],
            ),
            "Hgg": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Yukawa_NOTscalingbbH_couplingdependentBRs_Hgg.root"
                ]
                + base_args
                + base_args_scenario[Yukawa_NOTscalingbbH_couplingdependentBRs]
                + ["--PO isOnlyHgg=True"],
            ),
            "HZZ": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/smH_PTH_HZZ_processRenumbered_TK.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Yukawa_NOTscalingbbH_couplingdependentBRs_HZZ.root"
                ]
                + base_args
                + base_args_scenario[Yukawa_NOTscalingbbH_couplingdependentBRs]
                + ["--PO isOnlyHZZ=True"],
            ),
        },
        Top_scalingttH_floatingBRs: {
            "HggHZZHWWHtt": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHtt.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingttH_floatingBRs_HggHZZHWWHtt.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingttH_floatingBRs],
            ),
            "HggHZZHWWHttHbb": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHbb.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingttH_floatingBRs_HggHZZHWWHttHbb.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingttH_floatingBRs],
            ),
        },
        Top_scalingttH_couplingdependentBRs: {
            "HggHZZHWWHtt": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHtt.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingttH_couplingdependentBRs_HggHZZHWWHtt.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingttH_couplingdependentBRs],
            ),
            "HggHZZHWWHttHbb": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHbb.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingttH_couplingdependentBRs_HggHZZHWWHttHbb.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingttH_couplingdependentBRs],
            ),
        },
        Top_scalingbbHttH_floatingBRs: {
            "Hgg": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Top.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_floatingBRs_Hgg.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_floatingBRs],
            ),
            "HZZ": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/TK_Top.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_floatingBRs_HZZ.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_floatingBRs],
            ),
            "HggHZZHWWHtt": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHtt.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_floatingBRs_HggHZZHWWHtt.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_floatingBRs],
            ),
            "HggHZZHWWHttHbb": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHbb.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_floatingBRs_HggHZZHWWHttHbb.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_floatingBRs],
            ),
        },
        Top_scalingbbHttH_couplingdependentBRs: {
            "Hgg": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Top.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_Hgg.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
            "HZZ": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/TK_Top.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_HZZ.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
            "HbbVBF": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-21-020/testModel/model_combined_forComb.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_HbbVBF.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
            "HttBoost": Command(
                executable,
                "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt_forComb.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_HttBoost.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
            "HggHZZHWWHtt": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHtt.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_HggHZZHWWHtt.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
            "HggHZZHWWHttHbb": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHbb.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_HggHZZHWWHttHbb.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
            "HggHZZHWWHttHttBoost": Command(
                executable,
                "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHttBoost.txt",
                [
                    "-o DifferentialCombinationRun2/CombinedWorkspaces/TK/Top_scalingbbHttH_couplingdependentBRs_HggHZZHWWHttHttBoost.root"
                ]
                + base_args
                + base_args_scenario[Top_scalingbbHttH_couplingdependentBRs],
            ),
        },
    }

    for scenario in commands.keys():
        for decay_channel in commands[scenario].keys():
            commands[scenario][decay_channel].add_or_replace_option(
                "--PO channel={}".format(decay_channel.lower())
            )

    logger.info("Running {}".format(commands[args.model][args.category]))

    if args.validate:
        logger.info(commands[args.model][args.category].full_command)
    else:
        commands[args.model][args.category].run()


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

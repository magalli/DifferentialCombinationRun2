import os

# HggHZZHWWHtt

# Yukawa
print("HggHZZHWWHtt Yukawa")

input_card = "DifferentialCombinationRun2/CombinedCards/smH_PTH/HggHZZHWWHtt.txt"
output_card = (
    "DifferentialCombinationRun2/CombinedCards/TK/Yukawa_NOTscalingbbH_HggHZZHWWHtt.txt"
)

to_drop = [
    # Hgg
    "hgg_hgg_PTH_120p0_140p0",
    "hgg_hgg_PTH_140p0_170p0",
    "hgg_hgg_PTH_170p0_200p0",
    "hgg_hgg_PTH_200p0_250p0",
    "hgg_hgg_PTH_250p0_350p0",
    "hgg_hgg_PTH_350p0_450p0",
    "hgg_hgg_PTH_450p0_10000p0",
    # HZZ
    "hzz_hzz_PTH_120p0_200p0",
    "hzz_hzz_PTH_GT200",
    # HWW
    ".*?_120_200",
    ".*?_GT200",
    # Htt
    "htt_htt_PTH_120_200",
    "htt_htt_PTH_200_350",
    "htt_htt_PTH_350_450",
    "htt_htt_PTH_GT350",
    "htt_htt_PTH_GT450",
]

to_remove = [
    "120p0_140p0",
    "140p0_170p0",
    "170p0_200p0",
    "200p0_250p0",
    "250p0_350p0",
    "350p0_450p0",
    "450p0_10000p0",
]

nuis = ["ratesmH_PTH", "pdfindex"]
command = (
    "combineCards.py {} ".format(input_card)
    + " ".join(["--xc={}*".format(x) for x in to_drop])
    + " > {}".format(output_card)
)
print(command)
os.system(command)

# Fucking PGL che non sa rispettare le convenzioni dioporco
pg_names = {
    "smH_hww_PTH_0_15": "ggH_PTH_0_15",
    "smH_hww_PTH_15_30": "ggH_PTH_15_30",
    "smH_hww_PTH_30_45": "ggH_PTH_30_45",
    "smH_hww_PTH_45_80": "ggH_PTH_45_80",
    "smH_hww_PTH_80_120": "ggH_PTH_80_120",
    "smH_hww_PTH_120_200": "ggH_PTH_120_200",
    "smH_hww_PTH_200_350": "ggH_PTH_200_350",
    "smH_hww_PTH_GT350": "ggH_PTH_GT350",
}

new_lines = []
with open(output_card, "r") as f:
    lines = f.readlines()
    for line in lines:
        if line.startswith("imax"):
            line = "imax * number of bins\n"
            new_lines.append(line)
            continue
        if line.startswith("jmax"):
            line = "jmax * number of processes minus 1\n"
            new_lines.append(line)
            continue
        if line.startswith("kmax"):
            line = "kmax * number of nuisance parameter\n"
            new_lines.append(line)
            continue
        line = line.replace("ch1_", "")
        line = line.replace(" smH_PTH", " ggH_PTH")
        for k, v in pg_names.items():
            line = line.replace(k, v)
        # For some reason that I fail to understand, DifferentialCombinationRun2/CombinedCards/smH_PTH is attached to the path
        # Remove it
        line = line.replace("DifferentialCombinationRun2/CombinedCards/smH_PTH/", "")
        # Remove stuff like CMS_fake_syst_em_PTH_GT200 from hww
        if line.startswith("experimental"):
            new_words = []
            for word in line.split():
                if "GT200" not in word and "120_200" not in word:
                    new_words.append(word)
            line = " ".join(new_words) + "\n"
        if any(tr in line for tr in to_remove) and any(n in line for n in nuis):
            continue
        else:
            new_lines.append(line)
with open(output_card, "w") as f:
    for line in new_lines:
        f.write(line)


# Hgg

# Yukawa
print("Hgg Yukawa")

input_card = "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/Datacard_13TeV_differential_Pt.txt"
output_card = "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Yukawa.txt"

to_drop = [
    "hgg_PTH_120p0_140p0",
    "hgg_PTH_140p0_170p0",
    "hgg_PTH_170p0_200p0",
    "hgg_PTH_200p0_250p0",
    "hgg_PTH_250p0_350p0",
    "hgg_PTH_350p0_450p0",
    "hgg_PTH_450p0_10000p0",
]

to_remove = [
    "120p0_140p0",
    "140p0_170p0",
    "170p0_200p0",
    "200p0_250p0",
    "250p0_350p0",
    "350p0_450p0",
    "450p0_10000p0",
]

nuis = ["ratesmH_PTH", "pdfindex"]
command = (
    "combineCards.py {} ".format(input_card)
    + " ".join(["--xc={}*".format(x) for x in to_drop])
    + " > {}".format(output_card)
)
os.system(command)

new_lines = []
with open(output_card, "r") as f:
    lines = f.readlines()
    for line in lines:
        line = line.replace("ch1_", "")
        line = line.replace(" smH_PTH", " ggH_PTH")
        if any(tr in line for tr in to_remove) and any(n in line for n in nuis):
            continue
        else:
            new_lines.append(line)
with open(output_card, "w") as f:
    for line in new_lines:
        f.write(line)


# HZZ

# Yukawa
print("HZZ Yukawa")

input_card = "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/hzz4l_all_13TeV_xs_pT4l_bin_v3.txt"
output_card = "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/smH_PTH_HZZ_processRenumbered_TK.txt"

to_drop = ["hzz_PTH_120p0_200p0", "hzz_PTH_GT200"]
command = (
    "combineCards.py {} ".format(input_card)
    + " ".join(["--xc={}*".format(x) for x in to_drop])
    + " > {}".format(output_card)
)

os.system(command)

new_lines = []
with open(output_card, "r") as f:
    lines = f.readlines()
    for line in lines:
        line = line.replace("ch1_", "")
        line = line.replace(" smH_PTH", " ggH_PTH")
        new_lines.append(line)
with open(output_card, "w") as f:
    for line in new_lines:
        f.write(line)


# Top
print("Dumping Top cards")

input_cards = [
    "DifferentialCombinationRun2/CombinedCards/smH_PTH/HggHZZHWWHtt.txt",
    "DifferentialCombinationRun2/CombinedCards/smH_PTH/HggHZZHWWHttHbb.txt",
    "DifferentialCombinationRun2/CombinedCards/smH_PTH/HggHZZHWWHttHttBoost.txt",
    "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/Datacard_13TeV_differential_Pt.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/hzz4l_all_13TeV_xs_pT4l_bin_v3.txt",
]

output_cards = [
    "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHtt.txt",
    "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHbb.txt",
    "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHWWHttHttBoost.txt",
    "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Top.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/TK_Top.txt",
]

for input_card, output_card in zip(input_cards, output_cards):
    print(input_card)
    print(output_card)
    new_lines = []
    with open(input_card, "r") as f:
        lines = f.readlines()
        for line in lines:
            if line.startswith("imax"):
                line = "imax * number of bins\n"
                new_lines.append(line)
                continue
            if line.startswith("jmax"):
                line = "jmax * number of processes minus 1\n"
                new_lines.append(line)
                continue
            if line.startswith("kmax"):
                line = "kmax * number of nuisance parameter\n"
                new_lines.append(line)
                continue
            line = line.replace("ch1_", "")
            line = line.replace(" smH_PTH", " ggH_PTH")
            for k, v in pg_names.items():
                line = line.replace(k, v)
            new_lines.append(line)

    with open(output_card, "w") as f:
        for line in new_lines:
            f.write(line)

import argparse
import os
from datetime import datetime

from differential_combination.command.SMEFT.base_commands import (
    AsimovWithEnvelope,
    Observed,
    Asimov,
)
from differential_combination.utils import (
    setup_logging,
    extract_from_yaml_file,
    create_and_access_nested_dir,
)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Submit scans for SMEFT interpretations"
    )

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--observable",
        required=True,
        type=str,
        help="observable for which to run differential xs scans",
    )

    parser.add_argument(
        "--category", required=True, type=str, help="Category for which submit scans"
    )

    parser.add_argument(
        "--input-dir",
        type=str,
        help="Directory where the .root file containing the workspaces is stored",
    )

    parser.add_argument(
        "--output-dir",
        type=str,
        default="outputs",
        help="Directory where output files will be stored",
    )

    parser.add_argument(
        "--base-model",
        required=True,
        type=str,
        help="Path to the yaml file that was used to create the workspace",
    )

    parser.add_argument(
        "--submodel",
        required=True,
        type=str,
        help="If the name of a parameter, perform fit freezing all the others"
        "Otherwise it has to be the path to a yaml file with the name MODEL_SUBMODEL.yml",
    )

    parser.add_argument(
        "--skip-2d",
        action="store_true",
        required=False,
        default=False,
        help="Skip 2D scans",
    )

    parser.add_argument(
        "--skip-scans",
        action="store_true",
        required=False,
        default=False,
        help="Skip scans",
    )

    parser.add_argument(
        "--twod-only",
        action="store_true",
        required=False,
        default=False,
        help="Only run 2D scans",
    )

    parser.add_argument(
        "--global-fit-dir",
        type=str,
        default=None,
        help="Directory where the global fit is stored",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    logger.info("Starting scans submission for SMEFT interpretation")

    initial_path = os.path.abspath(os.getcwd())

    base_model_dct = extract_from_yaml_file(args.base_model)
    main_model_pois = list(base_model_dct.keys())
    model_name = args.base_model.split("/")[-1].split(".")[0]

    if args.input_dir.startswith("/"):
        input_dir = args.input_dir
    else:
        input_dir = "{}/{}".format(initial_path, args.input_dir)

    submodel_pois = None
    if args.submodel.endswith(".yml"):
        submodel_dct = extract_from_yaml_file(args.submodel)
        submodel_pois = [sp for sp in list(submodel_dct.keys()) if sp != "scan_ranges"]
        submodel_name = args.submodel.split("/")[-1].split(".")[0].split("_")[-1]
        pois_to_fit = submodel_pois

        # ranges
        substrings = [
            "{}={},{}".format(p, submodel_dct[p]["min"], submodel_dct[p]["max"])
            for p in submodel_pois
        ]
        set_parameter_ranges_string = "--setParameterRanges {}".format(
            ":".join(substrings)
        )
    else:
        submodel_name = "FreezeOthers"
        pois_to_fit = [args.submodel]
        set_parameter_ranges_string = ""

    # Pick routine and create
    which_routine = Observed
    if "Hgg" in args.category and "asimov" in args.category:
        which_routine = AsimovWithEnvelope
    elif "asimov" in args.category and "Hgg" not in args.category:
        which_routine = Asimov

    job_specs = {
        "220926Atlas": {
            "Hgg_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 4,
                "points_two_dim": 600,
                "split_points_two_dim": 4,
            },
            "HggHZZHWWHttHbbVBF_asimov": {
                "points_one_dim": 200,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            },
        }
    }

    try:
        points_one_dim = job_specs[model_name][args.category]["points_one_dim"]
        split_points_one_dim = job_specs[model_name][args.category][
            "split_points_one_dim"
        ]
        points_two_dim = job_specs[model_name][args.category]["points_two_dim"]
        split_points_two_dim = job_specs[model_name][args.category][
            "split_points_two_dim"
        ]
    except KeyError:
        logger.warning("No job specs found for {} {}".format(model_name, args.category))
        points_one_dim = 100
        split_points_one_dim = 2
        points_two_dim = 600
        split_points_two_dim = 2

    routine = which_routine(
        model_name,
        submodel_name,
        input_dir,
        args.category,
        main_model_pois=main_model_pois,
        pois_to_fit=pois_to_fit,
        submodel_pois=submodel_pois,
        skip_2d=args.skip_2d,
        skip_scans=args.skip_scans,
        set_parameter_ranges_string=set_parameter_ranges_string,
        twod_only=args.twod_only,
        statonly=True if "statonly" in args.category else False,
        skip_best=True if args.global_fit_dir else False,
        points_one_dim=points_one_dim,
        split_points_one_dim=split_points_one_dim,
        points_two_dim=points_two_dim,
        split_points_two_dim=split_points_two_dim,
    )

    logger.info("Built the following XSRoutine: \n{}".format(routine))
    logger.debug(
        "One line versions in case you want to copy:\n\n{}".format(
            "\n\n".join([c.full_command for c in routine.commands])
        )
    )
    # Create output subdirs and move to output dir
    if args.submodel.endswith(".yml"):
        output_path = "{}/{}/{}/{}/{}-{}".format(
            args.output_dir,
            args.observable,
            model_name,
            submodel_name,
            args.category,
            datetime.today().strftime("%Y%m%dxxx%H%M%S"),
        )
    else:
        output_path = "{}/{}/{}/FreezeOthers_{}/{}-{}".format(
            args.output_dir,
            args.observable,
            model_name,
            pois_to_fit[0],
            args.category,
            datetime.today().strftime("%Y%m%dxxx%H%M%S"),
        )

    new_output_path = create_and_access_nested_dir(output_path)
    if args.global_fit_dir:
        logger.info(
            "Copying global fit files from {} to {}".format(
                args.global_fit_dir, new_output_path
            )
        )
        if "Hgg" in args.category and "asimov" in args.category:
            for fl in [
                "higgsCombineAsimovBestFit.MultiDimFit.mH125.38.root",
                "higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                "higgsCombineAsimovPreFit.GenerateOnly.mH125.38.123456.root",
            ]:
                os.system(
                    "cp {}/{} {}".format(args.global_fit_dir, fl, new_output_path)
                )
        else:
            base_category = args.category.split("_")[0]
            global_fit_file = "higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                base_category
            )
            os.system(
                "cp {}/{} {}".format(
                    args.global_fit_dir, global_fit_file, new_output_path
                )
            )
    if args.debug:
        logger.debug("Removing {} as it won't be used".format(new_output_path))
        os.rmdir(new_output_path)

    # Run routine (submit jobs)
    if not args.debug:
        routine.run()


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

""" The datacards for HZZ 21-009 come separated per final state and per year.
This script, run from a DifferentialCombination_home directory, combines them into one.
"""
import subprocess
import os

def main():
    input_dir = "DifferentialCombinationRun2/Analyses/hig-21-009/forCombination"
    output_dir = "DifferentialCombinationRun2/CombinedCards"

    # In the following, when there is a dictionary, the key is the name in the OUTPUT while the value is the name in the INPUT

    bins = {
        "pT4l": {
            "0p0_15p0": "bin0",
            "15p0_30p0": "bin1",
            "30p0_45p0": "bin2",
            "45p0_80p0": "bin3",
            "80p0_120p0": "bin4",
            "120p0_200p0": "bin5",
            "200p0_350p0": "bin6",
            "350p0_450p0": "bin7",
            "GT450": "bin8"
        }
    }
    years = {
        "16": "2016", 
        "17": "2017", 
        "18": "2018"
        }
    categories = ["2e2muS", "4eS", "4muS"]
    # In these datacards, it is intended that whenever there is nothing it's smH, while GGH means ggH
    observables = {
        "smH_PTH": ("pT4l", ""),
        "ggH_PTH": ("pT4l", "_GGH"),
    }

    tmpl_process = "hzz_{observable}_{bin}_cat{category}_{year}"
    tmpl_txt = "{path}/datacard_{year}/hzz4l{prod_mode}_{category}_13TeV_xs_{observable}_{bin}_v3.txt"

    for obs in observables:
        cmd = ["combineCards.py"]
        for year in years:
            for cat in categories:
                for bin in bins[observables[obs][0]]:
                    cmd.append(
                        "{}={}".format(
                            tmpl_process.format(
                                observable=obs,
                                bin=bin,
                                category=cat,
                                year=year
                            ),
                            tmpl_txt.format(
                                path=input_dir,
                                year=years[year],
                                prod_mode=observables[obs][1],
                                category=cat,
                                observable=observables[obs][0],
                                bin=bins[observables[obs][0]][bin],
                            )
                        )
                    )

        cmd.append("> {}/{}/HZZ.txt".format(output_dir, obs))

        print "About to run command: {}".format(cmd)

        os.system(" ".join(cmd))



if __name__ == '__main__':
    main()
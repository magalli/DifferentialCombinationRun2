"""
TK's original model was not usable as it was for the full Run2 combination, due to the fact that in my case each decay channel has different boundaries.
The API to the stupid text2workspace thus has to be changed completely, with every argument referring to boundaries that now has to become 
a dictionary where the keys are the decay channels themselves.
These arguments seem to be:
- binBoundaries
- SMXS_of_input_ws
- correlationMatrix
- theoryUncertainties

As usual, the script is meant to be run from DifferentialCombination_home

Run with Python3
"""
from copy import deepcopy
import yaml
from yaml import Loader
import numpy as np
import json
import pickle as pkl
import scipy.interpolate as itr


def get_prediction(arr, mass, weights=None, interPRepl=None, massRepl=None):
    # Defined by Thomas in https://github.com/threiten/HiggsAnalysis-CombinedLimit/blob/d5d9ef377a7c69a8d4eaa366b47e7c81931e71d9/test/plotBinnedSigStr.py#L236
    # To be used with weights [1, 2.3, 1]
    nBins = len(arr)
    masses = [120.0, 125.0, 130.0]
    if weights is None:
        weights = np.ones(arr.shape[1])
    splines = []
    if arr.shape[1] == 1:
        if interPRepl is None or massRepl is None:
            raise ValueError(
                "If only one masspoint is given, interPRepl and massRepl must be provided!"
            )
        for i in range(nBins):
            splines.append(
                itr.UnivariateSpline(masses, interPRepl[i, :], w=weights, k=2)
            )

        return np.array(
            [
                splines[i](mass) - interPRepl[i, masses.index(massRepl)] + arr[i, 0]
                for i in range(nBins)
            ]
        )

    for i in range(nBins):
        splines.append(itr.UnivariateSpline(masses, arr[i, :], w=weights, k=2))

    return np.array([splines[i](mass) for i in range(nBins)])


def merge_bins(old_bins, old_edges, new_edges, over_bin_width=False):
    if not all(edge in old_edges for edge in new_edges):
        print(old_edges)
        print(new_edges)
        raise ValueError("New edges are not a subset of old edges")
    # Loop over pairs of consecutive edges in new_edges
    new_bins = []
    for first, second in zip(new_edges, new_edges[1:]):
        old_first_index = old_edges.index(first)
        old_second_index = old_edges.index(second)
        new_bins.append(sum(old_bins[old_first_index:old_second_index]))
    new_bins = np.array(new_bins)
    if over_bin_width:
        new_bins = new_bins / np.diff(new_edges)

    if len(new_bins) != len(new_edges) - 1:
        raise ValueError("Something went wrong")

    return new_bins


def main():
    max_to_tk = {
        "Hgg": "hgg",
        "HZZ": "hzz",
        "HWW": "hww",
        "Hbb": "hbb",
        "Htt": "htt",
        "HbbVBF": "hbbvbf",
        "HttBoost": "httboost",
        "Yukawa": "yukawa",
        "Top": "tophighpt",
    }

    binning = {
        "Yukawa": {
            "Hgg": [
                float(n) for n in [0, 5, 10, 15, 20, 25, 30, 35, 45, 60, 80, 100, 120]
            ],
            "HZZ": [float(n) for n in [0, 10, 20, 30, 45, 60, 80, 120]],
            "HWW": [float(n) for n in [0, 15, 30, 45, 80, 120]],
            "Htt": [0.0, 45.0, 80.0, 120.0],
        },
        "Top": {
            "Hgg": [
                float(n)
                for n in [
                    0,
                    5,
                    10,
                    15,
                    20,
                    25,
                    30,
                    35,
                    45,
                    60,
                    80,
                    100,
                    120,
                    140,
                    170,
                    200,
                    250,
                    350,
                    450,
                    550,  # this is because Thomas in the prediction considers the overflow bin as 100 GeV
                ]
            ],
            "HZZ": [float(n) for n in [0, 10, 20, 30, 45, 60, 80, 120, 200, 550]],
            "HWW": [float(n) for n in [0, 15, 30, 45, 80, 120, 200, 350, 550]],
            "Htt": [0.0, 45.0, 80.0, 120.0, 140.0, 170.0, 200.0, 350.0, 450.0, 550.0],
            "Hbb": [0.0, 200.0, 300.0, 450.0, 650.0, 750.0],
            "HbbVBF": [0.0, 450.0, 500.0, 550.0, 600.0, 675.0, 800.0],
            "HttBoost": [
                0.0,
                450.0,
                600.0,
                700.0,
            ],  # fake last edge so we can use predictions that were already there before 03.11.2022
        },
    }

    # From https://www.hepdata.net/record/sandbox/1652460569
    ob = np.array(
        [
            0.73036,
            1.45,
            1.4984,
            1.3485,
            1.1458,
            1.0285,
            0.87579,
            0.72506,
            0.52321,
            0.33926,
            0.21509,
            0.13407,
            0.086204,
            0.057235,
            0.033481,
            0.01814,
            0.0060278,
            0.0017241,
            0.00073824,
        ]
    )
    hgg_br = 0.0023
    # IMPORTANT!
    ob = ob / hgg_br
    ob = ob / 1000  # convert to pb
    ob_absolute = ob * np.diff(binning["Top"]["Hgg"])
    observations = {}
    observations["Top"] = {}
    observations["Top"]["Hgg"] = ob_absolute

    # specific for Hbb and HttBoost
    pt_ggH_granular_binning = [
        0.0,
        5.0,
        10.0,
        15.0,
        20.0,
        25.0,
        30.0,
        35.0,
        45.0,
        60.0,
        80.0,
        100.0,
        120.0,
        140.0,
        170.0,
        200.0,
        250.0,
        300.0,
        350.0,
        400.0,
        450.0,
        500.0,
        550.0,
        600.0,
        650.0,
        700.0,
        750.0,
        10000.0,
    ]
    with open(
        "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TheoreticalPredictions/production_modes/ggH_MoreGranular/theoryPred_Pt_2018_ggHMoreGranular.pkl",
        "rb",
    ) as f:
        arr = pkl.load(f)
    ob_from_granular = get_prediction(arr, 125.38, weights=[1, 2.3, 1])
    ob_from_granular = ob_from_granular / hgg_br
    ob_from_granular = ob_from_granular / 1000  # convert to pb
    ob_from_granular_absolute = ob_from_granular * np.diff(pt_ggH_granular_binning)

    observations["Top"]["Hbb"] = merge_bins(
        ob_from_granular_absolute,
        pt_ggH_granular_binning,
        binning["Top"]["Hbb"],
        over_bin_width=False,
    )

    observations["Top"]["HttBoost"] = merge_bins(
        ob_from_granular_absolute,
        pt_ggH_granular_binning,
        binning["Top"]["HttBoost"],
        over_bin_width=False,
    )

    # specific for HbbVBF
    pt_ggH_evenmoregranular_binning = [
        0.0,
        5.0,
        10.0,
        15.0,
        20.0,
        25.0,
        30.0,
        35.0,
        45.0,
        60.0,
        80.0,
        100.0,
        120.0,
        140.0,
        170.0,
        200.0,
        250.0,
        300.0,
        350.0,
        400.0,
        450.0,
        500.0,
        550.0,
        600.0,
        650.0,
        675.0,
        700.0,
        750.0,
        800.0,
        1200.0,
    ]
    with open(
        "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TheoreticalPredictions/production_modes/ggH_EvenMoreGranular/theoryPred_Pt_2018_ggHEvenMoreGranular.pkl",
        "rb",
    ) as f:
        arr = pkl.load(f)
    ob_from_evenmoregranular = get_prediction(arr, 125.38, weights=[1, 2.3, 1])
    ob_from_evenmoregranular = ob_from_evenmoregranular / hgg_br
    ob_from_evenmoregranular = ob_from_evenmoregranular / 1000  # convert to pb
    ob_from_evenmoregranular_absolute = ob_from_evenmoregranular * np.diff(
        pt_ggH_evenmoregranular_binning
    )

    observations["Top"]["HbbVBF"] = merge_bins(
        ob_from_evenmoregranular_absolute,
        pt_ggH_evenmoregranular_binning,
        binning["Top"]["HbbVBF"],
        over_bin_width=False,
    )

    for cat in binning["Top"].keys():
        if cat in ["Hgg", "Hbb", "HttBoost", "HbbVBF"]:
            continue
        observations["Top"][cat] = merge_bins(
            # ob_absolute,
            ob_from_granular_absolute,
            binning["Top"]["Hgg"],
            binning["Top"][cat],
            over_bin_width=False,
        )

    observations["Yukawa"] = {}
    # observations["Yukawa"]["Hgg"] = ob_absolute[: len(binning["Yukawa"]["Hgg"]) - 1]
    observations["Yukawa"]["Hgg"] = ob_from_granular_absolute[
        : len(binning["Yukawa"]["Hgg"]) - 1
    ]
    for cat in binning["Yukawa"].keys():
        if cat == "Hgg":
            continue
        observations["Yukawa"][cat] = merge_bins(
            # ob_absolute,
            ob_from_granular_absolute,
            binning["Yukawa"]["Hgg"],
            binning["Yukawa"][cat],
            over_bin_width=False,
        )

    # ttH
    print("Doing ttH")
    pt_ttH_granular_binning = [
        0.0,
        5.0,
        10.0,
        15.0,
        20.0,
        25.0,
        30.0,
        35.0,
        45.0,
        60.0,
        80.0,
        100.0,
        120.0,
        140.0,
        170.0,
        200.0,
        250.0,
        300.0,
        350.0,
        400.0,
        450.0,
        500.0,
        550.0,
        600.0,
        650.0,
        700.0,
        750.0,
        10000.0,
    ]

    with open(
        "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TheoreticalPredictions/production_modes/ttH/theoryPred_Pt_2018_ttHJetToGG-MoreGranular.pkl",
        "rb",
    ) as f:
        tth_ob = get_prediction(pkl.load(f), 125.38, [1.0, 2.3, 1.0])
    print(f"Original values for ttH: {','.join([str(i) for i in tth_ob])}")
    tth_ob = tth_ob / hgg_br
    tth_ob = tth_ob / 1000  # convert to pb
    tth_ob_absolute = tth_ob * np.diff(pt_ttH_granular_binning)
    tth_xs = {}
    for cat in ["Hgg", "HWW", "HZZ", "Htt", "Hbb", "HttBoost"]:
        tth_xs[cat] = merge_bins(
            tth_ob_absolute,
            pt_ttH_granular_binning,
            binning["Top"][cat],
            over_bin_width=False,
        )

    # specific for HbbVBF
    pt_ttH_evenmoregranular_binning = pt_ggH_evenmoregranular_binning
    with open(
        "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TheoreticalPredictions/production_modes/ttH/theoryPred_Pt_2018_EvenMoreGranularttHJetToGG.pkl",
        "rb",
    ) as f:
        tth_ob_emg = get_prediction(pkl.load(f), 125.38, [1.0, 2.3, 1.0])
    print(
        f"Original values for ttH even more granular: {','.join([str(i) for i in tth_ob_emg])}"
    )
    tth_ob_emg = tth_ob_emg / hgg_br
    tth_ob_emg = tth_ob_emg / 1000  # convert to pb
    tth_ob_emg_absolute = tth_ob_emg * np.diff(pt_ttH_evenmoregranular_binning)
    tth_xs["HbbVBF"] = merge_bins(
        tth_ob_emg_absolute,
        pt_ggH_evenmoregranular_binning,
        binning["Top"]["HbbVBF"],
        over_bin_width=False,
    )

    # weird format required by TK's model
    to_dump = {}
    for cat in ["Hgg", "HWW", "HZZ", "Htt", "Hbb", "HttBoost", "HbbVBF"]:
        to_dump[max_to_tk[cat]] = {}
        for left_edge, xs in zip(binning["Top"][cat], tth_xs[cat]):
            to_dump[max_to_tk[cat]][left_edge] = xs

    with open(
        "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TKPredictions/ttH.json",
        "w",
    ) as f:
        json.dump(to_dump, f, indent=4)

    for scenario in ["Yukawa", "Top"]:
        print("Scenario: {}".format(scenario))

        # bin boundaries
        print("Doing binning")
        base_dir = "DifferentialCombinationRun2/metadata/TK/bin_boundaries"
        to_dump = {}
        for decay_channel in binning[scenario]:
            to_dump[max_to_tk[decay_channel]] = binning[scenario][decay_channel]
        print(to_dump)
        with open("{}/{}.yml".format(base_dir, scenario), "w") as f:
            yaml.dump(to_dump, f, default_flow_style=False)

        # SMXS
        print("Doing SMXS")
        base_dir = "DifferentialCombinationRun2/metadata/TK/SMXS"
        to_dump = {}
        for decay_channel in observations[scenario]:
            smxs = list([float(n) for n in observations[scenario][decay_channel]])
            to_dump[max_to_tk[decay_channel]] = smxs
        with open("{}/{}.yml".format(base_dir, scenario), "w") as f:
            yaml.dump(to_dump, f, default_flow_style=False)

        # correlation matrices paths
        print("Doing correlation matrices")
        base_dir = "DifferentialCombinationRun2/metadata/TK/correlation_matrices_paths"
        to_dump = {}
        for decay_channel in binning[scenario]:
            to_dump[
                max_to_tk[decay_channel]
            ] = "DifferentialCombinationRun2/TKPredictions/scalecorrelations_{}/scalecorrelations_{}/corrmat_{}_{}.txt".format(
                decay_channel,
                max_to_tk[scenario],
                max_to_tk[scenario],
                max_to_tk[scenario],
            )
        with open("{}/{}.yml".format(base_dir, scenario), "w") as f:
            yaml.dump(to_dump, f, default_flow_style=False)

        # theory uncertainties
        print("Doing theory uncertainties")
        base_dir = "DifferentialCombinationRun2/metadata/TK/theory_uncertainties_paths"
        to_dump = {}
        for decay_channel in binning[scenario]:
            to_dump[
                max_to_tk[decay_channel]
            ] = "DifferentialCombinationRun2/TKPredictions/scalecorrelations_{}/scalecorrelations_{}/errors_{}_{}.txt".format(
                decay_channel,
                max_to_tk[scenario],
                max_to_tk[scenario],
                max_to_tk[scenario],
            )
        with open("{}/{}.yml".format(base_dir, scenario), "w") as f:
            yaml.dump(to_dump, f, default_flow_style=False)


if __name__ == """__main__""":
    main()

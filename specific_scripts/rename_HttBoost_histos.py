import os
import ROOT

# Renaming histos
print("Renaming histos")
input_dirs = [
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/2016/common",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/2017/common",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/2018/common",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_LeadJetPt_NoOverLap/V1_diff_jpt_dr0p5/2016/common",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_LeadJetPt_NoOverLap/V1_diff_jpt_dr0p5/2017/common",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_LeadJetPt_NoOverLap/V1_diff_jpt_dr0p5/2018/common",
]

for input_dir in input_dirs:
    root_files = [
        "{}/{}".format(input_dir, f)
        for f in os.listdir(input_dir)
        if f.endswith(".root")
    ]
    for fname in root_files:
        f = ROOT.TFile.Open(fname, "UPDATE")

        dir_names = [k.GetName() for k in list(f.GetListOfKeys())]

        for dn in dir_names:
            d = f.Get(dn)
            elements_names = [k.GetName() for k in list(d.GetListOfKeys())]
            for en in elements_names:
                if "OutsideAcceptance" in en:
                    print("Renaming {} in {}".format(en, fname))
                    e = d.Get(en)
                    new_name = en.replace(
                        "OutsideAcceptance", "OutsideAcceptanceHttBoost"
                    )
                    e.SetName(new_name)
                    f.cd(dn)
                    e.Write()
        f.Write()

# Renaming cards
print("Renaming cards")

cards = [
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_LeadJetPt_NoOverLap/V1_diff_jpt_dr0p5/hig-21-017_jpt.txt",
]

for card in cards:
    with open(card, "r") as f:
        lines = f.readlines()

    with open(card, "w") as f:
        for line in lines:
            if "OutsideAcceptance" in line:
                line = line.replace("OutsideAcceptance ", "OutsideAcceptanceHttBoost ")
            f.write(line)

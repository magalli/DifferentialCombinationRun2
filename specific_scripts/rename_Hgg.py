"""
In the conventions twiki it is not specified wheter or not observables with non-decimal bin boundaries
should have 'p0' in the name.
In Hgg we put it while in the other analyses it doesn't seem to be there.
What we do here is to remove p0 from the gen-bin names, so that they can be easily combined with the others.
It has to be run from the root level of DifferentialCombinationRun2.
"""
import re


def main():
    new_lines = []
    with open("Analyses/hig-19-016/outdir_differential_Pt/Datacard_13TeV_differential_Pt.txt", "r") as f:
        old_lines = f.readlines()

    for line in old_lines:
        poi_list = re.findall(' smH_PTH_.+?p0_.+?p0 ', line)
        if poi_list:
            for poi in poi_list:
                line = line.replace(poi, poi.replace("p0", ""))
        new_lines.append(line)

    with open("Analyses/hig-19-016/outdir_differential_Pt/Datacard_13TeV_differential_Pt_renamed.txt", "w") as f:
        f.writelines(new_lines)

if __name__ == "__main__":
    main()
"""
Taken entirely from TK.
Just order from lowest to highest the processes with ggH, xH, smH found in the datacard.
Fucking Combine dioporco.
"""

def process_sorter(process):
    if not isinstance(process, str):
        process = process[0]

    ret = 100000
    if process.startswith( 'ggH' ):
        ret -= 10000
    elif process.startswith( 'xH' ):
        ret -= 20000
    elif process.startswith( 'smH' ):
        ret -= 30000

    if process.startswith( 'ggH' ) or process.startswith( 'xH' ) or process.startswith( 'smH' ):
        components = process.split('_')
        if len(components) >= 3:
            leftBound = int( components[2].replace('GT','').replace('p0', '') )
            ret += leftBound
    else:
        ret += 1000
    return ret


def main():
    datacard_file = "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/Analyses/hig-21-009/forCombination/smH_PTH_HZZ.txt"

    with open( datacard_file, 'r' ) as datacard_fp:
        lines = datacard_fp.readlines()

    first_match = True
    for i_line, line in enumerate(lines):
        if line.startswith('process '):
            if first_match:
                names = line.split()[1:]
                first_match = False
                i_name_line = i_line
            else:
                numbers = line.split()[1:]
                i_number_line = i_line
                break
    else:
        raise RuntimeError(
            'Reached end of file while searching two lines '
            'that start with \'process\'; datacard {0} is faulty.'
            .format(datacard_file)
            )

    signals = []
    bkgs    = []
    for process in list(set(names)):
        if process.startswith('ggH') or process.startswith('xH') or process.startswith('smH'):
            signals.append(process)
        else:
            bkgs.append(process)

    signals.sort( key=process_sorter )
    bkgs.sort( key=process_sorter )

    number_dict = {}
    for iSignal, signal in enumerate( signals ):
        number_dict[signal] = -iSignal
    for iBkg, bkg in enumerate( bkgs ):
        number_dict[bkg] = iBkg + 1

    new_number_line = '{0:50}'.format('process')
    for name in names:
        new_number_line += '{0:<35}'.format( number_dict[name] )
    lines[i_number_line] = new_number_line + '\n'

    # Write to file
    out_file = datacard_file.replace( '.txt', '_processesRenumbered.txt' )
    with open(out_file, 'w') as out_fp:
        out_fp.write(''.join(lines))



if __name__ == "__main__":
    main()
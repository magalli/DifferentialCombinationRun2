import os
import ROOT

# Renaming histos
print("Renaming histos")
input_dirs = [
    "DifferentialCombinationRun2/Analyses/hig-20-015/HiggsPt",
    "DifferentialCombinationRun2/Analyses/hig-20-015/NJets",
]

for input_dir in input_dirs:
    root_files = [
        "{}/{}".format(input_dir, f)
        for f in os.listdir(input_dir)
        if f.endswith(".root")
    ]
    for fname in root_files:
        f = ROOT.TFile.Open(fname, "UPDATE")

        dir_names = [k.GetName() for k in list(f.GetListOfKeys())]

        for dn in dir_names:
            d = f.Get(dn)
            elements_names = [k.GetName() for k in list(d.GetListOfKeys())]
            for en in elements_names:
                if "OutsideAcceptance" in en:
                    e = d.Get(en)
                    new_name = en.replace("OutsideAcceptance", "OutsideAcceptanceHtt")
                    e.SetName(new_name)
                    f.cd(dn)
                    e.Write()
        f.Write()

# Renaming cards
print("Renaming cards")

cards = [
    "DifferentialCombinationRun2/Analyses/hig-20-015/HiggsPt/HTT_Run2FinalCard_HiggsPt_NoReg.txt",
    "DifferentialCombinationRun2/Analyses/hig-20-015/NJets/HTT_Run2FinalCard_NJets_NoReg.txt",
]

for card in cards:
    with open(card, "r") as f:
        lines = f.readlines()

    with open(card, "w") as f:
        for line in lines:
            if "OutsideAcceptance" in line:
                line = line.replace("OutsideAcceptance ", "OutsideAcceptanceHtt ")
            f.write(line)

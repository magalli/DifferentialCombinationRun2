"""
To be run with python3
"""
import numpy as np
import scipy.interpolate as itr
import pickle as pkl
import json
import pprint
from copy import deepcopy


def get_prediction(arr, mass, weights=None, interPRepl=None, massRepl=None):
    # Defined by Thomas in https://github.com/threiten/HiggsAnalysis-CombinedLimit/blob/d5d9ef377a7c69a8d4eaa366b47e7c81931e71d9/test/plotBinnedSigStr.py#L236
    # To be used with weights [1, 2.3, 1]
    nBins = len(arr)
    masses = [120.0, 125.0, 130.0]
    if weights is None:
        weights = np.ones(arr.shape[1])
    splines = []
    if arr.shape[1] == 1:
        if interPRepl is None or massRepl is None:
            raise ValueError(
                "If only one masspoint is given, interPRepl and massRepl must be provided!"
            )
        for i in range(nBins):
            splines.append(
                itr.UnivariateSpline(masses, interPRepl[i, :], w=weights, k=2)
            )

        return np.array(
            [
                splines[i](mass) - interPRepl[i, masses.index(massRepl)] + arr[i, 0]
                for i in range(nBins)
            ]
        )

    for i in range(nBins):
        splines.append(itr.UnivariateSpline(masses, arr[i, :], w=weights, k=2))

    return np.array([splines[i](mass) for i in range(nBins)])


def list_of_edges_to_list_of_pairs(list_of_edges):
    return [(l, r) for l, r in zip(list_of_edges[:-1], list_of_edges[1:])]


def merge_bins(granular_binning, granular_predictions, gen_binning, poi_binning):
    map = {}
    granular_pairs = list_of_edges_to_list_of_pairs(granular_binning)
    gen_pairs = list_of_edges_to_list_of_pairs(gen_binning)
    poi_pairs = list_of_edges_to_list_of_pairs(poi_binning)

    for gen_pair in gen_pairs:
        gen_left = gen_pair[0]
        gen_right = gen_pair[1]
        associated_poi_pairs = []
        for i, poi_pair in enumerate(poi_pairs):
            poi_left = poi_pair[0]
            poi_right = poi_pair[1]
            if (poi_left >= gen_left and poi_left < gen_right) or (
                poi_right > gen_left and poi_right <= gen_right
            ):
                associated_poi_pairs.append(poi_pair)

        if len(associated_poi_pairs) == 1:
            map[gen_pair] = [(associated_poi_pairs[0], 1.0)]
        else:
            """
            In order to compute weights a la Marini, we change the left edge of the 
            first poi pair to the left edge of the gen pair and the right edge of the
            last poi pair to the right edge of the gen pair. We can then compute the 
            weights using the granular prediction
            """
            fake_associated_pairs = deepcopy(associated_poi_pairs)
            fake_associated_pairs[0] = (gen_left, fake_associated_pairs[0][1])
            fake_associated_pairs[-1] = (fake_associated_pairs[-1][0], gen_right)
            gen_left_index = granular_binning.index(gen_left)
            gen_right_index = granular_binning.index(gen_right)
            weights = []
            for fake_pair in fake_associated_pairs:
                fake_left = fake_pair[0]
                fake_right = fake_pair[1]
                fake_left_granular_index = granular_binning.index(fake_left)
                fake_right_granular_index = granular_binning.index(fake_right)
                # note that using left:right instead of left:right+1 takes care
                # of the fact that len(granular_binning) = len(granular_predictions) + 1
                w = np.sum(
                    granular_predictions[
                        fake_left_granular_index:fake_right_granular_index
                    ]
                ) / np.sum(granular_predictions[gen_left_index:gen_right_index])
                weights.append(w)
            map[gen_pair] = list(zip(associated_poi_pairs, weights))
    return map


def make_strings(map, observable):
    """
    map is a dictionary of the form 
    {
        "decay_channel1": {
            (a, d): [((a, b), w1), ((b, c), w2), ((c, d), w3)],
            (d, f): [((d, e), w1), ((e, f), w2)],
        },
    }
    """
    string_templates = {
        "pt": {
            "hgg": {
                "simple": "\"--PO 'map=.*hgg.*/smH_PTH_{}:r_smH_PTH_{}[1.0,-3.0,4.0]'\",",
                "multi": '"--PO \'map=.*hgg.*/smH_PTH_{}:merge_{}_hgg=expr::merge_{}_hgg(\\"{}\\",{})\'",',
            },
            "hzz": {
                "simple": "\"--PO 'map=.*hzz.*/smH_PTH_{}:r_smH_PTH_{}[1.0,-3.0,4.0]'\",",
                "multi": '"--PO \'map=.*hzz.*/smH_PTH_{}:merge_{}_hzz=expr::merge_{}_hzz(\\"{}\\",{})\'",',
            },
            "hww": {
                "simple": "\"--PO 'map=.*hww.*/*H_hww_PTH_{}:r_smH_PTH_{}[1.0,-3.0,4.0]'\",",
                "multi": '"--PO \'map=.*hww.*/*H_hww_PTH_{}:merge_{}_hww=expr::merge_{}_hww(\\"{}\\",{})\'",',
            },
            "htt": {
                "simple": "\"--PO 'map=.*htt.*/.*H.*PTH_{}:r_smH_PTH_{}'\",",
                "multi": '"--PO \'map=.*htt.*/.*H.*PTH_{}.*:merge_{}_htt=expr::merge_{}_htt(\\"{}\\",{})\'",',
            },
            # "hbb": {
            #    "simple": "\"--PO 'map=.*hbb.*/ggH_PTH_{}:r_smH_PTH_{}[1,-20.0,20.0]'\",",
            #    "multi": '"--PO \'map=.*hbb.*/ggH_PTH_{}:merge_{}_hbb=expr::merge_{}_hbb(\\"{}\\",{})\'",',
            # },
            # we won't add hbbvbf because we can simply copy from the individual one, it's faster
            "httboost": {
                "simple": "\"--PO 'map=.*httboost.*/.ggH_PTH_{}:r_smH_PTH_{}'\",",
                "multi": '"--PO \'map=.*httboost.*/.ggH_PTH_{}:merge_{}_httboost=expr::merge_{}_httboost(\\"{}\\",{})\'",',
            },
        }
    }
    strings = {}
    templates = string_templates[observable]
    for decay_channel, decay_channel_map in map.items():
        if decay_channel not in ["hbbvbf"]:
            strings[decay_channel] = []
            merge_funcs_counter = 0
            for gen_pair, poi_pairs in decay_channel_map.items():
                if len(poi_pairs) == 1:
                    poi_pair = poi_pairs[0][0]
                    if decay_channel in ["hgg", "hzz"]:
                        process_name = f"{str(gen_pair[0]).replace('.', 'p')}_{str(gen_pair[1]).replace('.', 'p')}"
                    else:
                        process_name = (
                            f"{str(int(gen_pair[0]))}_{str(int(gen_pair[1]))}"
                        )
                    poi_name = f"{str(int(poi_pair[0]))}_{str(int(poi_pair[1]))}"
                    strings[decay_channel].append(
                        templates[decay_channel]["simple"].format(
                            process_name, poi_name
                        )
                    )
                else:
                    if decay_channel in ["hgg", "hzz"]:
                        process_name = f"{str(gen_pair[0]).replace('.', 'p')}_{str(gen_pair[1]).replace('.', 'p')}"
                    else:
                        process_name = (
                            f"{str(int(gen_pair[0]))}_{str(int(gen_pair[1]))}"
                        )
                    weights = [str(w) for w in [p[1] for p in poi_pairs]]
                    weird_string = "+".join(
                        [f"@{i}*{w}" for i, w in enumerate(weights)]
                    )
                    if observable == "pt":
                        pois_string = ",".join(
                            [
                                f"r_smH_PTH_{str(int(pair[0]))}_{str(int(pair[1]))}"
                                for pair in [p[0] for p in poi_pairs]
                            ]
                        )
                        strings[decay_channel].append(
                            templates[decay_channel]["multi"].format(
                                process_name,
                                merge_funcs_counter,
                                merge_funcs_counter,
                                weird_string,
                                pois_string,
                            )
                        )
                    merge_funcs_counter += 1

    # it's faster to replace the GTs rather then insert one thousand billion if statements
    for decay_channel, decay_channel_strings in strings.items():
        strings[decay_channel] = [
            s.replace("r_smH_PTH_650_10000", "r_smH_PTH_GT650")
            .replace("r_smH_PTH_450_10000", "r_smH_PTH_GT450")
            .replace("H_hww_PTH_350_10000", "H_hww_PTH_GT350")
            .replace(".*H.*PTH_450_10000", ".*H.*PTH_G.450")
            .replace("ggH_PTH_650_10000", "ggH_PTH_GT650")
            .replace("smH_PTH_450p0_1200p0", "smH_PTH_450p0_10000p0")
            .replace("smH_PTH_200p0_1200p0", "smH_PTH_GT200")
            .replace("H_hww_PTH_350_1200", "H_hww_PTH_GT350")
            .replace(".*H.*PTH_450_1200", ".*H.*PTH_G.450")
            .replace("ggH_PTH_600_1200", "ggH_PTH_GT600")
            for s in decay_channel_strings
        ]

    return strings


def main():
    hgg_br = 0.0023
    mass = 125.38
    weights = [1.0, 2.3, 1.0]

    pt_granular_binning = [
        0.0,
        5.0,
        10.0,
        15.0,
        20.0,
        25.0,
        30.0,
        35.0,
        45.0,
        60.0,
        80.0,
        100.0,
        120.0,
        140.0,
        170.0,
        200.0,
        250.0,
        300.0,
        350.0,
        400.0,
        450.0,
        500.0,
        550.0,
        600.0,
        650.0,
        675.0,
        700.0,
        750.0,
        800.0,
        1200.0,
    ]

    pt_gen_binnings = {
        "hgg": [
            0.0,
            5.0,
            10.0,
            15.0,
            20.0,
            25.0,
            30.0,
            35.0,
            45.0,
            60.0,
            80.0,
            100.0,
            120.0,
            140.0,
            170.0,
            200.0,
            250.0,
            350.0,
            450.0,
            # 10000.0,
            1200.0,  # to make it work with HbbVBF
        ],
        "hzz": [0.0, 10.0, 20.0, 30.0, 45.0, 60.0, 80.0, 120.0, 200.0, 1200.0],
        "hww": [0.0, 15.0, 30.0, 45.0, 80.0, 120.0, 200.0, 350.0, 1200.0],
        "htt": [0.0, 45.0, 80.0, 120.0, 140.0, 170.0, 200.0, 350.0, 450.0, 1200.0],
        # "hbb": [0.0, 200.0, 300.0, 450.0, 650.0, 1200.0],
        "hbbvbf": [0.0, 450.0, 500.0, 550.0, 600.0, 675.0, 800.0, 1200.0],
        "httboost": [0.0, 450.0, 600.0, 1200.0],
    }

    pt_poi_binning = [
        0.0,
        5.0,
        10.0,
        15.0,
        20.0,
        25.0,
        30.0,
        35.0,
        45.0,
        60.0,
        80.0,
        100.0,
        120.0,
        140.0,
        170.0,
        200.0,
        250.0,
        350.0,
        450.0,
        500.0,
        550.0,
        600.0,
        675.0,
        800.0,
        1200.0,
    ]

    print("\nWorking with pt\n")
    with open(
        "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TheoreticalPredictions/production_modes/ggH_EvenMoreGranular/theoryPred_Pt_2018_ggHEvenMoreGranular.pkl",
        "rb",
    ) as f:
        arr = pkl.load(f)
    pt_pred = get_prediction(arr, mass, weights=weights)

    pt_maps = {}
    for dc, bins in pt_gen_binnings.items():
        pt_maps[dc] = merge_bins(pt_granular_binning, pt_pred, bins, pt_poi_binning)
    # print(pprint.pformat(pt_maps, indent=4, depth=6))

    pt_strings = make_strings(pt_maps, "pt")
    for dc, strings in pt_strings.items():
        print(dc)
        for string in strings:
            print(string)


if __name__ == "__main__":
    main()

import os
import ROOT

old_name = "OutsideAcceptance"
new_name = "OutsideAcceptanceHZZ"

# Renaming pdfs
print("Renaming processes")
input_dirs = [
    "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l",
    "DifferentialCombinationRun2/Analyses/hig-21-009/njets_pt30_eta4p7",
    "DifferentialCombinationRun2/Analyses/hig-21-009/rapidity4l",
    "DifferentialCombinationRun2/Analyses/hig-21-009/pTj1"
]
dc_dirs = [
    "datacard_2016",
    "datacard_2017",
    "datacard_2018",
]

for input_dir in input_dirs:
    for dc_dir in dc_dirs:
        root_files = [
            "{}/{}/{}".format(input_dir, dc_dir, f)
            for f in os.listdir("{}/{}".format(input_dir, dc_dir))
            if f.endswith(".root") and f.startswith("hzz4l")
        ]
        for fname in root_files:
            print("Analyzing {}".format(fname))
            f = ROOT.TFile(fname)
            w = f.w
            pdf = w.pdf(old_name)
            if pdf != None:
                pdf.SetNameTitle(new_name, new_name)
            w.writeToFile(fname)

# Renaming cards
print("Renaming cards")

cards = [
    "DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/hzz4l_all_13TeV_xs_pT4l_bin_v3.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-009/njets_pt30_eta4p7/hzz4l_all_13TeV_xs_njets_pt30_eta4p7_bin_v3.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-009/rapidity4l/hzz4l_all_13TeV_xs_rapidity4l_bin_v3.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-009/pTj1/hzz4l_all_13TeV_xs_pTj1_bin_v3.txt"
]

for card in cards:
    with open(card, "r") as f:
        lines = f.readlines()

    with open(card, "w") as f:
        for line in lines:
            if "OutsideAcceptance" in line:
                line = line.replace("OutsideAcceptance ", "OutsideAcceptanceHZZ ")
            f.write(line)

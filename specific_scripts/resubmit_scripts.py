import argparse
import os


def parse_args():
    parser = argparse.ArgumentParser(description="Resubmit jobs.")
    parser.add_argument("--input-dir", type=str, required=True, help="Input directory.")
    return parser.parse_args()


def main(args):
    print("Moving to {}".format(args.input_dir))
    os.chdir(args.input_dir)
    shell_scripts = [f for f in os.listdir(".") if f.endswith(".sh") and f.startswith("job")]

    options = "--mem=20G --partition=long --time=23:00:00"
    for shell_script in shell_scripts:
        to_point = shell_script.split('.')[0]
        try:
            log_file = [f for f in os.listdir('.') if f.startswith("{}_".format(to_point)) or f == "{}.log".format(to_point)][0]
        except IndexError:
            print("No log file found for {}".format(shell_script))
            pass
        with open(log_file) as f:
            content = f.read()
        if "DUE TO TIME LIMIT" in content:
            print("Resubmitting {} because it failed due to time limit".format(shell_script))
            os.system("sbatch {} {}".format(options, shell_script))


if __name__ == "__main__":
    main(parse_args())

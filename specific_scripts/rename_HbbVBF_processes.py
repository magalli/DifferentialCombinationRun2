"""
To be run with python3
"""
import ROOT

to_replace = [
    "testModel_2016APV.root",
    "testModel_2016.root",
    "testModel_2017.root",
    "testModel_2018.root",
]
replaced = [
    "testModel_2016APV_forComb.root",
    "testModel_2016_forComb.root",
    "testModel_2017_forComb.root",
    "testModel_2018_forComb.root",
]
prefix = "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/Analyses/hig-21-020/testModel"
card_path = (
    "DifferentialCombinationRun2/Analyses/hig-21-020/testModel/model_combined.txt"
)
output_path = "DifferentialCombinationRun2/Analyses/hig-21-020/testModel/model_combined_withpaths.txt"

# First, add absolute prefix
new_lines = []
with open(card_path, "r") as f:
    lines = f.readlines()
    for l in lines:
        if l.startswith("imax"):
            line = "imax * number of bins\n"
            new_lines.append(line)
            continue
        if l.startswith("jmax"):
            line = "jmax * number of processes minus 1\n"
            new_lines.append(line)
            continue
        if l.startswith("kmax"):
            line = "kmax * number of nuisance parameter\n"
            new_lines.append(line)
            continue
        for rp, the_replaced in zip(to_replace, replaced):
            l = l.replace(rp, "{}/{}".format(prefix, the_replaced))
        new_lines.append(l)

# Change process names
lines_starting_with_bin = []
lines_starting_with_bin_index = []
lines_starting_with_process = []
lines_starting_with_process_index = []
for i, nl in enumerate(new_lines):
    if nl.startswith("bin"):
        lines_starting_with_bin.append(nl)
        lines_starting_with_bin_index.append(i)
    if nl.startswith("process"):
        lines_starting_with_process.append(nl)
        lines_starting_with_process_index.append(i)
good_line_bin = lines_starting_with_bin[1]
good_line_bin_index = lines_starting_with_bin_index[1]
bins = good_line_bin.split()[1:]
good_line_process = lines_starting_with_process[0]
processes = good_line_process.split()[1:]
good_line_process_index = lines_starting_with_process_index[0]

maps = {
    "ptbin0mjjbin0ggf": "ggH_PTH_450_500",
    "ptbin1mjjbin0ggf": "ggH_PTH_500_550",
    "ptbin2mjjbin0ggf": "ggH_PTH_550_600",
    "ptbin3mjjbin0ggf": "ggH_PTH_600_675",
    "ptbin4mjjbin0ggf": "ggH_PTH_675_800",
    "ptbin5mjjbin0ggf": "ggH_PTH_800_1200",
}

new_processes = []
for b, p in zip(bins, processes):
    has_something = False
    for k, v in maps.items():
        if b.startswith(k) and p == "ggF":
            new_processes.append(v)
            has_something = True
            break
    if not has_something:
        new_processes.append(p)

new_process_line = "process " + " ".join(new_processes) + "\n"
new_lines[good_line_process_index] = new_process_line

print("Number oof processes: {}".format(len(processes)))
print("Number of new processes: {}".format(len(new_processes)))

with open(output_path, "w") as f:
    for l in new_lines:
        f.write(l)

# Now rename in the root files
for rp, the_replaced in zip(to_replace, replaced):
    print("Renaming in {}".format(rp))
    ws_name = rp.replace(".root", "")
    root_file_name = "{}/{}".format(prefix, rp)
    replaced_file_name = "{}/{}".format(prefix, the_replaced)
    f = ROOT.TFile(root_file_name)
    freplaced = ROOT.TFile(replaced_file_name, "RECREATE")
    w = f.Get(ws_name)
    # first rename data
    print("\n\nRenaming data")
    data = w.allData()
    data_list = list(data)
    for obj in data_list:
        full_old_name = obj.GetName()
        for first_part_of_bin_name, process in maps.items():
            if full_old_name.startswith(first_part_of_bin_name):
                print("Replacing ggF with {} in {}".format(process, full_old_name))
                new_name = full_old_name.replace("ggF", process)
                obj.SetName(new_name)
                break
    w.writeToFile(replaced_file_name)

    # rename pdfs
    print("\n\nRenaming pdfs")
    for bin_name, new_process_name in zip(bins, new_processes):
        for first_part_of_bin_name in maps.keys():
            if bin_name.startswith(
                first_part_of_bin_name
            ) and new_process_name.startswith("ggH"):
                print(
                    "bin_name: {}, first_part_of_bin_name: {}, process: {}".format(
                        bin_name, first_part_of_bin_name, new_process_name
                    )
                )
                old_name = "{}_ggF".format(bin_name)
                new_name = "{}_{}".format(bin_name, maps[first_part_of_bin_name])
                pdf = w.pdf(old_name)
                if pdf != None:
                    print(
                        "Found {} in {}:{}, renaming".format(
                            old_name, root_file_name, ws_name
                        )
                    )
                    pdf.SetNameTitle(new_name, new_name)
                w.writeToFile(replaced_file_name)

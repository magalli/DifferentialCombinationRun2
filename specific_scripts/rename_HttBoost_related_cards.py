cards = (
    "DifferentialCombinationRun2/CombinedCards/smH_PTH/HggHZZHWWHttHbbVBFHttBoost.txt",
    "DifferentialCombinationRun2/CombinedCards/smH_PTH/HggHZZHWWHttHttBoost.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt_forComb.txt"
)

for card in cards:
    with open(card, "r") as f:
        lines = f.readlines()

    with open(card, "w") as f:
        for line in lines:
            if "$MASS" in line:
                line = line.replace("$MASS", "125")
            f.write(line)

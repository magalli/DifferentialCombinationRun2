"""
PGL l'ha fatta fuori dal secchio come al solito.
As we already noticed, in HWW datacards the name of the processes do not respect the conventions. I managed to fix this problem in the signal strength fitting part,
but for what concerns the TK model this can't happen, since TK stuff relies on the signal processes starting with either "ggH" or "xH".
It is possible to rename HWW processes names, but then we also need to add histograms with the same name to the already present ROOT files.
"""
import os
import ROOT


input_dir = (
    "DifferentialCombinationRun2/Analyses/hig-19-002/ptH_for_differential_combination"
)
root_files = [
    "{}/{}".format(input_dir, f) for f in os.listdir(input_dir) if f.endswith(".root")
]

pg_names = {
    "smH_hww_PTH_0_15": "ggH_PTH_0_15",
    "smH_hww_PTH_15_30": "ggH_PTH_15_30",
    "smH_hww_PTH_30_45": "ggH_PTH_30_45",
    "smH_hww_PTH_45_80": "ggH_PTH_45_80",
    "smH_hww_PTH_80_120": "ggH_PTH_80_120",
    "smH_hww_PTH_120_200": "ggH_PTH_120_200",
    "smH_hww_PTH_200_350": "ggH_PTH_200_350",
    "smH_hww_PTH_GT350": "ggH_PTH_GT350",
}

for fname in root_files:
    print("Processing {}".format(fname))
    f = ROOT.TFile.Open(fname, "UPDATE")

    dir_names = [k.GetName() for k in list(f.GetListOfKeys())]

    for dn in dir_names:
        d = f.Get(dn)
        elements_names = [k.GetName() for k in list(d.GetListOfKeys())]
        for en in elements_names:
            for old_pattern, new_pattern in pg_names.items():
                if old_pattern in en:
                    print(
                        "Found {} in {}, replacing with {}".format(
                            old_pattern, en, new_pattern
                        )
                    )
                    e = d.Get(en)
                    new_name = en.replace(old_pattern, new_pattern)
                    e.SetName(new_name)
                    f.cd(dn)
                    e.Write()
    f.Write()
